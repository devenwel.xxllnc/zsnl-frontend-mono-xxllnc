# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.59.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.58.1...@zaaksysteem/main@0.59.0) (2022-01-26)


### Bug Fixes

* **Case:** fix merge issues and small review findings ([9a0619b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9a0619b59c122ddfb4ea519d0040186fdf6118ed))
* **Case:** MINTY-7105 - fix missing htmlEmailTemplateName in communication ([903aa27](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/903aa271b5d0648df3bb53285e9242d41a78fcf7))
* **Case:** MINTY-7112 - Fix role value when editing subject relation ([233b8e2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/233b8e23d38a9b5021ccb761d3622935803cc7a6))
* **Case:** MINTY-7113 - Disallow adding objects v1 when no related casetypes ([a066407](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a066407415e9056f007dccb419c1dca5fa1eb2be))
* **Case:** MINTY-7115 - Show time for planned e-mails ([3d0e365](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3d0e365804dcf999162f9029dd1c5f5102568569))
* **Case:** MINTY-7334 - Fix v1 case fetch from ObjectForm and remove store ([85cb6a3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/85cb6a32fe28d3906d1233a863c1c971fcf08d58))
* **Case:** MINTY-7462 - Use specific case type version instead of latest ([9e8a72e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9e8a72e9a7effcd33e2ce44b832c0acc9be6111f))
* **ContactView:** Implement case status icons for archival_statusses transfer and destroy ([dcc17f5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/dcc17f55db9d20665eaf5711a45a2489c884bc8e))
* **ContactView:** MINTY-7376 - Show loading message on more than just initial load ([773fbb2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/773fbb2be2c08a3ee9e9fabd326686cd4bb9848b))
* **ContactView:** MINTY-7377 - Display the contact instead of the logged in user ([c8579da](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c8579da1570dd6101eda0d239a123eb20aa69ec9))
* **ContactView:** MINTY-7547 - Fix employee transform for employees without roles ([b320e34](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b320e3434b3651f8572c8212057b31a0b5d71647))
* **ContactView:** MINTY-7562 - Fix preferred contact channel ([78ecb31](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/78ecb31d3359b9afe0619424673fdc7d2a3734dc))
* **ContactView:** MINTY-7565 - Fix cases on address view visibility and text ([e129e1a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e129e1a9d2f0dbdb788965e1a84f7c2431712a89))
* **ContactView:** MINTY-7642 - Fix display and saving of notification settings ([b9f1699](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b9f16991ed21a6182f010e6b4c17d6d84b7cbd00))
* **ContactView:** MINTY-7667 - Fix date display for persons ([ab52ac5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ab52ac582c6e3a59724d60ba892d17f9e5f6f5f8))
* **ContactView:** MINTY-7668 - Fix date display ([a1f9bb1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a1f9bb1bfb65c331c32be8b923356ad238366408))
* **ContactView:** MINTY-7668 - Fix date display for organisations ([6f4ebef](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6f4ebef2622838cd6d38ea847666a1b3bc8fd435))
* **ContactView:** MINTY-7843 - Allow PersonalNumber to only be requested by 'Persoonsverwerker' ([95c2f6d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/95c2f6d86250c1a56b1f1eb6fa3e85eda8dd8f57))
* **ContactView:** MINTY-7855 - Fix subtitle for signature upload ([c988fef](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c988fefdb5f81b10ec7ac3c012023758f1e20a50))
* **ContactView:** MINTY-7915 - Show related object for employee and fix for person and organization ([239bfc8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/239bfc838c6b031ec98daafc26c2f19e359c7e41))
* **ContactView:** MINTY-7949 - Display specific indicator for the deceased ([0dcf504](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0dcf5042cfbf77ece1321ef4f120399db092e1b8))
* **ContactView:** MINTY-8134 - Fix icons for to-be-destroyed-cases without destruction date ([15f8d7c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/15f8d7c4192908212a3da7f8c32ddbebcaac31f2))
* **ContactView:** MINTY-8134 - Fix icons for to-be-destroyed-cases without destruction date ([9da7d22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9da7d229f54a2c68b2dad32cb653f94433e88df4))
* **ContactView:** MINTY-8221 - Display country as part of foreign address ([ef935f4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef935f4e479c79841f0cc6085ae467ab8237d515))
* **Dashboard:** MINTY-7513 - Fix case number filter in task widget ([fe7f3db](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fe7f3dbe3e47ddbe2ce92efd183e588f1bfc4cd4))
* **DocumentIntake:** MINTY-6466 - fix error when saving assign dialog ([fbdf6c1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fbdf6c19be5dd3d3b9012f8b5d7520264557eb0e))
* **DocumentIntake:** MINTY-6971 - fix not being able to open import contact dialog ([3702e49](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3702e4949d34b73f5b06a3e8952a6588adb94c5a))
* **Geo:** MINTY-6540 - adjust map styling in object form ([3cbdfef](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3cbdfef00a9ab28c6e9b37b16cb287d4cb478815))
* **Intake:** MINTY-7048 - fix save error when assigning ([2688b8b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2688b8b6b1ca687b4015a0f37133eb379c789889))
* **Intake:** MINTY-7834 - fix error when adding document to case from intake ([79f98cd](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/79f98cd7e42ad5c23295010fb857dcc0b2e6e897))
* **ObjectForm:** MINTY-8185 - Use specific object type version when editing ([328ee33](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/328ee339c9b3191257b78fa1e91b7bbb9404056d))
* **ObjectForm:** MINTY-8384 - Disallow editing of inactive objects for normal users ([731a797](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/731a7972e34e1e86b8ab09ec2a19f53f4678c43e))
* **ObjectForm:** MINTY-8398 - Fetch case type version that the case is based on ([01f398b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/01f398b30191ad2516431349d360b8017fd75ba2))
* **ObjectForm:** MINTY-8398 - Fix prefill of mapped custom field values ([084bc74](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/084bc74023cd773d00f664e1443dcb885424ba65))
* **Related User:** MINTY-7974 - remove spaces and _ in magic strings ([aab2b24](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/aab2b2427745cb88721cb2cf3c11cedb826b3add))
* **Related User:** MINTY-7974 - replace spaces in magic strings with _ ([45c4f09](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/45c4f09e1800c401d42d48d05bf4850a3053808c))
* **SortableTable:** MINTY-7005 - change logic for sorting values & dates ([069628c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/069628c79b892cd3ea051be220b013a7a4f4cca0))
* **Tasks:** MINTY-6153 - fix table height for Tasks widget ([ebed9b1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ebed9b1ea8f7494c416ad5dc303126416c34911a))
* **Tasks:** MINTY-7212 - fix parameter error for tasks ([e499cea](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e499cea01a596c5b0fd66e4735f0ba13bc6f046d))
* **Tasks:** MINTY-7212 - fix parameter error for tasks ([98eee87](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/98eee875e9116e35fe11801639e01731b7455f6d))
* **Tasks:** MINTY-7513 - fix prettier ([cd9a3ba](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/cd9a3ba96969c4b5f327ca8304c3dee12549514c))


### Features

* add row count functionality to infinite scroll / data provider ([b1b4e51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b1b4e5138b1019fff82914baf3e802202995b373))
* **BreadcrumbBar:** MINTY-7787 - Disable options that have not been implemented yet ([f8b4e9f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f8b4e9f4844a296b4bc32c3b66510912b1efe8a3))
* **Case:** Add relation tab with the related cases tables ([c4b3925](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c4b39253759c0c3831c61b9d287c70fc3afd061b))
* **CaseCreate:** MINTY-6048 - add ability to prefill requestor ([e4ac430](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e4ac430d863e0ec37fe0dc51aaedfa81501c32ac))
* **CaseCreate:** MINTY-8136 - start case create with different contact channels ([e413c95](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e413c959d82873e70c9315b8050978fd0d65b9ff))
* **Case:** MINTY-7105 - Implement empty case relation tab ([9b283ae](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9b283ae85e3b8fa4e2b8aa1f6d7adfc96ebbedba))
* **Case:** MINTY-7111 - Add table planned cases to relation tab ([82080b6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/82080b60e390704d28564ec174083ed733342041))
* **Case:** MINTY-7111 - Remove download button from planned cases ([295a284](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/295a284842ffa68c6c4c71183542cd0e3deee323))
* **Case:** MINTY-7112 - Add contacts to relations tab ([d8c8148](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d8c8148dbf3cb387e5866f335ddbd977585561de))
* **Case:** MINTY-7115 - Add planned emails to relations tab ([7732403](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/77324031c7f0de2250fd8573989d0f673a828d05))
* **Case:** MINTY-7468 - Allow role to be used multiple times for contact relations ([41f3e00](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/41f3e0059e10acc94f6f00123441a3a0c6b2b503))
* **Case:** MINTY-7963 - Implement contact importer in relation tab ([cad4334](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/cad433479c7608a2f6178acae2a27a1514f59bf4))
* **Case:** MINTY-8179 - Implement get_case_basic ([ec2654e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ec2654ecd7f1e2aac15bbf122a66a3e0204ed4a4))
* **CaseRelations:** MINTY-7259 - implement sorting in case relations table ([ab6bcb3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ab6bcb3eb574d1420ac18a53b4fc46fa430831f2))
* **Communication:** MINTY-4853 - disable option to create PIP message if disabled in case type ([f2db651](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f2db65143e49335bf6529acfaf7e88e90541b850))
* **ContactView:** MINTY-5796 - Location tab in contact view ([34718f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/34718f2258032d43726e9258e29e9d1d2c98fa33))
* **ContactView:** MINTY-6052 - add contact view setup ([f5ad9b4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f5ad9b4472f52cc0936ba7781c4943ab3e4b3a5e))
* **contactView:** MINTY-6167 - communications tab in new contact view ([61fe065](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/61fe065403e38e0a91af0dafe0d74a3254e44579))
* **contactView:** MINTY-6168 - Move location tab to new contact view ([226d43a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/226d43a1877f767ba8f6ffbe5e45f97344111761))
* **ContactView:** MINTY-6169 - Relationship tab in new contact view ([f4dfb2b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f4dfb2b043e7123b3acd3fc384f5df1ab2eddc5f))
* **ContactView:** MINTY-6980, MINTY-6982: add Timeline for ContactView, add export feature ([31b9738](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/31b97383504c928b9ce6d21d9d01bb657c94bf49))
* **ContactView:** MINTY-6980, MINTY-6982: add Timeline for ContactView, add export feature ([48e02c1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/48e02c19efbb8a4e320583064c1bce3d4964a2ef))
* **ContactView:** MINTY-6980, MINTY-6982: add Timeline for ContactView, add export feature ([795682b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/795682b957d92dba94879966ece1f0bd780dbe6f))
* **Contactview:** MINTY-7096 - Add overview and decorate table ([3c36e5e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3c36e5e0c4a18ec874776d6268f74f0d1327c3cc))
* **ContactView:** MINTY-7376, MINTY-7377, MINTY-7378 - rework Cases Table with autoloading, filtering, sorting from backend ([f83b0f9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f83b0f9cc8340efbdef7e92ed3984079ae4a0fdd))
* **ContactView:** MINTY-7547 - Show inactive warning for employees without roles ([0f9e4b4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0f9e4b41772ade3aece8d904c8e457a4349b532e))
* **ContactView:** MINTY-7671 - Add breadcrumb ([c242d13](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c242d13579659b115b7ec892446f19796817fead))
* **ContactView:** MINTY-7671 - Add breadcrumb ([531b09b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/531b09b6a8b595feb707aa470364aca56971742e))
* **ContactView:** MINTY-7684 - Refresh contactview after save ([84816b9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/84816b96a3e4d73111de35edaf026c0173eedb69))
* **ContactView:** MINTY-7684 - Refresh contactview after save ([09660a8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/09660a8be13869bd5236993cf4276619a171ee34))
* **ContactView:** MINTY-7706 - Add alternative authentication ([85795ad](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/85795adfc574c98e22d53e069bc0d746cbedd162))
* **ContactView:** MINTY-7739 - add support for custom object type in contactview search bar ([1f236a0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1f236a0d75ba20204ce4fb358de54dd98fc2a819))
* **ContactView:** MINTY-7739 - add support for custom object type in contactview search bar ([d76b839](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d76b83924040184e20a4998abbc4f96662fe5d57))
* **ContactView:** MINTY-7772 - Indicate whether contact is inactive ([4d3ccd3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4d3ccd32287766cdb5f8a7cf3a17fdd35073aba7))
* **ContactView:** MINTY-7849 - Add text filter to cases view ([1e2b9a3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1e2b9a3ed9bfc79b753ce9f4f4685b088f9234aa))
* **ContactView:** MINTY-7967 - allow admins to edit anonymous user checkbox, disable fields when needed ([110dc5d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/110dc5d7014feeebd34b22632d0509fcb9cb0a22))
* **ContactView:** MINTY-8222 - Implement secret and internaleNote notifications ([1555457](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1555457e5bd052bb4da00c3dfb2643eb3ff13259))
* **Documentintake:** MINTY-7791 - Add button to navigate to v1 intake ([6a9ba4a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6a9ba4a84f4acdaf1e3bad62aaf379aaeb86932f))
* **Intake:** MINTY-6443 - add option to reject document in intake ([88f50eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/88f50eb53bf3d7a7b79eac8c450c0f7480fac966))
* **Intake:** MINTY-6733 - add document preview functionality ([9adb63b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9adb63b775f42df0bd6930974ab3df1b537a86a1))
* **Intake:** MINTY-7571 - Use backend for sorting, filtering, paging in document intake ([785033f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/785033f078101c9a0939f2a612490ac591621bf5))
* **Intake:** MINTY-7571 - Use backend for sorting, filtering, paging in document intake - change loading mechanism on page 0 ([f2779a9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f2779a97478932a7121ec1958afbdfda339a9fc3))
* **Intake:** MINTY-7626 - add default date when adding document to case from intake ([fd529a6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fd529a65dedec8327f4f1541820dba80ccde0d48))
* **Intake:** MINTY-7626 - add default date when adding document to case from intake ([543cade](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/543cade1e8ec3ed5843a4522bd353997641435bb))
* **Intake:** MINTY-8234 - change add to case dialog to show documentnames in case of multiple ([c7ec5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c7ec5e500fddeee1afea25685479fbd9d46aafa4))
* **NavigationBar:** MINTY-6445 - Change link to new documentintake ([ddb971e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ddb971e20711d47e2f000a8d7670c06b0a83a0e3))
* **ObjectForm:** MINTY-7460 - Also support hardcoded values being prefilled on casetype attribute mapping ([6db0e63](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6db0e63aaecb12669413c60481275e5990d5b2bc))
* **ObjectForm:** MINTY-7460 - Prefill values based on casetype attribute mapping ([6852990](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/685299021f2e83d7f41a6e5527385518a1181ee1))
* **ObjectForm:** MINTY-7461 - Support prefill of certain magicstrings ([574e333](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/574e3338f1c120cabf1395be3a5afd4ad8809de3))
* **ObjectForm:** MINTY-7466 - Hide system attributes ([821bfdf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/821bfdf78b9d6241778212d11fdedb547b152519))
* **ObjectForm:** MINTY-8212 - Do not relate object to case when creating ([f1348bc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f1348bc6f2f295004e5ea8f2e7a99d03f94958ea))
* **ObjectForm:** MINTY-8227 - Support multiple object attributes with the same object type ([aac2712](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/aac271282dc4703c22421264d9acc597c4b3e00e))
* **Object:** MINTY-8057 - Allow objects to be activated and deactivated ([21261b2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/21261b2435e70fe26eec2331b57030e2f8a33b7b))
* **Objects:** MINTY-7929 - add support for multivalue object attribute ([9d35477](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9d354772c8ae64e0dd9818752ea67e6fccf1be8b))
* **ObjectView:** MINTY-5177 - Allow editing ([2702760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2702760f4eb375b1d4acde2db041b6c3395a066a))
* **ObjectView:** MINTY-6092 - Use version indepedent uuid for custom object view ([7152e38](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7152e386bab6eff3eb2b6779ac5308b30ff98ff9))
* **ObjectView:** MINTY-7467 - Support system attributes ([4c0a745](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4c0a745d54faeff9f2c3c92d78756ba8817e4f78))
* **Search:** MINTY-7847, MINTY-7853 - add filters to main search, open in new window ([9cb3fbd](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9cb3fbdf17bc533aaffa4fc03ebab9e778d054d9))
* **Tasks:** MINTY-7248 - change filtering to server side for Tasks Dashboard Widget ([5cadb28](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5cadb28d58e1f755fcf217a5ad10475c5122afce))
* **Tasks:** MINTY-7248 - change filtering to server side for Tasks Dashboard Widget - add completed filter ([45e7c9a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/45e7c9a413b7a1c0dd343984fa7150fbfb8f302d))
* **Tasks:** MINTY-7248 - change filtering to server side for Tasks Dashboard Widget - refactor ([8c5d4e1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8c5d4e1938f6c2f8b882646de0b503be0a07e007))
* **Timeline:** add timeline page for cases ([ca7e98c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ca7e98c5bb3dff9376cb19a397d7dc3d683ddb9a))
* **Timeline:** MINTY-5337 - add Timeline component ([5ad8858](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ad885846dfa18a48da2245ee482cd49984fac15))
* **Timeline:** MINTY-7845 - Fix timeline icons and titles, and prefix case descriptions ([9d342a6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9d342a6fe789d128c6b3e2ad1e0fb8046ee047cc))
* **Timeline:** MINTY-8235 - add types filter to case timeline ([87aff18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/87aff18ca80da0fb2572fccf1b41f1d56a736a6d))


### Reverts

* Revert "add temp Case Timeline debugging" ([5d62cee](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5d62cee77be287f59a73041479418db8ea6365c8))





## [0.58.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.58.0...@zaaksysteem/main@0.58.1) (2020-12-21)

**Note:** Version bump only for package @zaaksysteem/main





# [0.58.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.6...@zaaksysteem/main@0.58.0) (2020-12-18)


### Features

* **ObjectForm:** MINTY-5537 - Prefill case data when creating a new object ([996ee0f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/996ee0f7422a81e84b04c77140b3f148af1c8908))





## [0.57.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.5...@zaaksysteem/main@0.57.6) (2020-12-18)

**Note:** Version bump only for package @zaaksysteem/main





## [0.57.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.4...@zaaksysteem/main@0.57.5) (2020-12-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.57.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.3...@zaaksysteem/main@0.57.4) (2020-12-10)

**Note:** Version bump only for package @zaaksysteem/main





## [0.57.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.2...@zaaksysteem/main@0.57.3) (2020-12-04)

**Note:** Version bump only for package @zaaksysteem/main





## [0.57.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.1...@zaaksysteem/main@0.57.2) (2020-12-03)


### Bug Fixes

* **Tasks:** MINTY-5672 - change timing of middleware calls ([a685e0a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a685e0af8cb4c7dc2cfee78945e4339bb54fdb00))





## [0.57.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.57.0...@zaaksysteem/main@0.57.1) (2020-11-25)

**Note:** Version bump only for package @zaaksysteem/main





# [0.57.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.56.2...@zaaksysteem/main@0.57.0) (2020-11-20)


### Features

* **ObjectForm:** MINTY-5648 - Include all case relations when updating objects ([853dab3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/853dab3cc0cad370d6fb9c8962989898029eef88))





## [0.56.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.56.1...@zaaksysteem/main@0.56.2) (2020-11-19)

**Note:** Version bump only for package @zaaksysteem/main





## [0.56.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.56.0...@zaaksysteem/main@0.56.1) (2020-11-18)

**Note:** Version bump only for package @zaaksysteem/main





# [0.56.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.55.0...@zaaksysteem/main@0.56.0) (2020-11-13)


### Features

* **ContactView:** MINTY-5151 - iframe relations ([698f05f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/698f05f))





# [0.55.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.54.1...@zaaksysteem/main@0.55.0) (2020-11-12)


### Features

* **ContactView:** MINTY-5165 - iframe custom fields contact tab ([e1f6310](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1f6310))





## [0.54.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.54.0...@zaaksysteem/main@0.54.1) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/main





# [0.54.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.6...@zaaksysteem/main@0.54.0) (2020-11-11)


### Features

* **Tasks:** MINTY-5546 - add ability to order and filter columns ([292f80a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/292f80a))





## [0.53.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.5...@zaaksysteem/main@0.53.6) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/main





## [0.53.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.4...@zaaksysteem/main@0.53.5) (2020-10-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.53.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.3...@zaaksysteem/main@0.53.4) (2020-10-26)

**Note:** Version bump only for package @zaaksysteem/main





## [0.53.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.2...@zaaksysteem/main@0.53.3) (2020-10-26)


### Bug Fixes

* **ContactFinder:** MINTY-5313 - fix broken contact finder configs ([73f6c23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/73f6c23))





## [0.53.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.1...@zaaksysteem/main@0.53.2) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/main





## [0.53.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.53.0...@zaaksysteem/main@0.53.1) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/main





# [0.53.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.52.0...@zaaksysteem/main@0.53.0) (2020-10-15)


### Features

* **Intake:** MINTY-5324 - add extension hints and label to Document Intake ([e6d0049](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e6d0049))
* **ObjectView:** MINTY-5260 - related subjects in object overview ([3fb33fc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3fb33fc))





# [0.52.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.51.2...@zaaksysteem/main@0.52.0) (2020-10-08)


### Features

* **Dashboard:** MINTY-4976 - Assignee filter option ([5804b60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5804b60))
* **TasksWidget:** MINTY-4976 - add Tasks widget features with overlay form ([ba9c2bf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ba9c2bf))
* **TasksWidget:** MINTY-4976 - change outputFormat to config ([069fd79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/069fd79))
* **TasksWidget:** MINTY-4976 - fix crash bug with department finder labels ([b372f6e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b372f6e))
* **TasksWidget:** MINTY-4976 - fix keywords filtering, too many post calls ([482ab37](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/482ab37))
* **TasksWidget:** MINTY-4976 - misc MR related code improvements ([8a27f33](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8a27f33))





## [0.51.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.51.1...@zaaksysteem/main@0.51.2) (2020-10-06)

**Note:** Version bump only for package @zaaksysteem/main





## [0.51.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.51.0...@zaaksysteem/main@0.51.1) (2020-10-02)

**Note:** Version bump only for package @zaaksysteem/main





# [0.51.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.6...@zaaksysteem/main@0.51.0) (2020-09-30)


### Features

* **Intake:** MINTY-5066 & MINTY-5069 - change intake behaviour in Pip/Intake ([aaa8bdf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/aaa8bdf))
* **Intake:** MINTY-5069 - change IconButton to A ([7b8e700](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7b8e700))





## [0.50.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.5...@zaaksysteem/main@0.50.6) (2020-09-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.4...@zaaksysteem/main@0.50.5) (2020-09-29)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.3...@zaaksysteem/main@0.50.4) (2020-09-29)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.2...@zaaksysteem/main@0.50.3) (2020-09-25)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.1...@zaaksysteem/main@0.50.2) (2020-09-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.50.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.50.0...@zaaksysteem/main@0.50.1) (2020-09-18)

**Note:** Version bump only for package @zaaksysteem/main





# [0.50.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.49.2...@zaaksysteem/main@0.50.0) (2020-09-17)


### Features

* **Dashboard:** MINTY-4975 - Widget search ([d2c4a5c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d2c4a5c))





## [0.49.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.49.1...@zaaksysteem/main@0.49.2) (2020-09-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.49.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.49.0...@zaaksysteem/main@0.49.1) (2020-09-17)

**Note:** Version bump only for package @zaaksysteem/main





# [0.49.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.48.2...@zaaksysteem/main@0.49.0) (2020-09-16)


### Features

* **Main:** MINTY-4759 - add menu items to main menu bar ([8ed4876](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8ed4876))





## [0.48.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.48.1...@zaaksysteem/main@0.48.2) (2020-09-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.48.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.48.0...@zaaksysteem/main@0.48.1) (2020-09-15)

**Note:** Version bump only for package @zaaksysteem/main





# [0.48.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.47.0...@zaaksysteem/main@0.48.0) (2020-09-15)


### Features

* **Intake:** MINTY-5020 - change text on Add To Case dialog ([7b01b36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7b01b36))
* **Main:** MINTY-4808 - create case from + button ([2f1fbe9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2f1fbe9))





# [0.47.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.46.0...@zaaksysteem/main@0.47.0) (2020-09-14)


### Features

* **Dashboard:** Minty 4889 - Be able to delete task widget from dashboard ([a048c23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a048c23))





# [0.46.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.45.1...@zaaksysteem/main@0.46.0) (2020-09-04)


### Features

* **ObjectView:** Implement readOnly mode for objectView ([3ae03a7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3ae03a7))





## [0.45.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.45.0...@zaaksysteem/main@0.45.1) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/main





# [0.45.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.44.0...@zaaksysteem/main@0.45.0) (2020-09-03)


### Features

* **ObjectView:** MINTY-4867, MINTY-4877 - add relationships overview to Object View ([57ef51d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/57ef51d))





# [0.44.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.4...@zaaksysteem/main@0.44.0) (2020-09-03)


### Features

* **Main:** MINTY-4758 - start new action from Object View ([1867c5b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1867c5b))





## [0.43.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.3...@zaaksysteem/main@0.43.4) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/main





## [0.43.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.2...@zaaksysteem/main@0.43.3) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.43.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.1...@zaaksysteem/main@0.43.2) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.43.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.43.0...@zaaksysteem/main@0.43.1) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/main





# [0.43.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.42.0...@zaaksysteem/main@0.43.0) (2020-08-27)


### Features

* **Dashboard:** MINTY-4800 - add Tasks Dashboard widget merged ([df0dffc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/df0dffc))





# [0.42.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.41.0...@zaaksysteem/main@0.42.0) (2020-08-21)


### Features

* **objectForm:** MINTY-4749 - Add support for currency, email, webaddress, iban, date ([f18d1ea](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f18d1ea))





# [0.41.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.40.2...@zaaksysteem/main@0.41.0) (2020-08-20)


### Features

* **Main:** MINTY-4752 - add Search from Object View ([ab753fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ab753fb))





## [0.40.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.40.1...@zaaksysteem/main@0.40.2) (2020-08-19)

**Note:** Version bump only for package @zaaksysteem/main





## [0.40.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.40.0...@zaaksysteem/main@0.40.1) (2020-08-17)

**Note:** Version bump only for package @zaaksysteem/main





# [0.40.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.40.0) (2020-08-11)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.39.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.39.0) (2020-08-07)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.38.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.38.0) (2020-08-07)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.37.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.37.0) (2020-08-06)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





## [0.36.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.2...@zaaksysteem/main@0.36.3) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/main





## [0.36.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.1...@zaaksysteem/main@0.36.2) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/main





## [0.36.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.36.0...@zaaksysteem/main@0.36.1) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/main





# [0.36.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.9...@zaaksysteem/main@0.36.0) (2020-07-23)


### Features

* **Intake:** Add support in External Contact for importing companies ([c43bdeb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c43bdeb))





## [0.35.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.8...@zaaksysteem/main@0.35.9) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.7...@zaaksysteem/main@0.35.8) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.6...@zaaksysteem/main@0.35.7) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.5...@zaaksysteem/main@0.35.6) (2020-07-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.4...@zaaksysteem/main@0.35.5) (2020-07-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.3...@zaaksysteem/main@0.35.4) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.2...@zaaksysteem/main@0.35.3) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.1...@zaaksysteem/main@0.35.2) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/main





## [0.35.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.35.0...@zaaksysteem/main@0.35.1) (2020-07-14)

**Note:** Version bump only for package @zaaksysteem/main





# [0.35.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.5...@zaaksysteem/main@0.35.0) (2020-07-09)


### Features

* **External Contacts:** MINTY-4347: add External Contact search/import for persons ([ef299fe](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef299fe))





## [0.34.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.4...@zaaksysteem/main@0.34.5) (2020-07-09)

**Note:** Version bump only for package @zaaksysteem/main





## [0.34.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.3...@zaaksysteem/main@0.34.4) (2020-07-02)

**Note:** Version bump only for package @zaaksysteem/main





## [0.34.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.2...@zaaksysteem/main@0.34.3) (2020-06-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.34.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.1...@zaaksysteem/main@0.34.2) (2020-06-26)

**Note:** Version bump only for package @zaaksysteem/main





## [0.34.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.34.0...@zaaksysteem/main@0.34.1) (2020-06-25)

**Note:** Version bump only for package @zaaksysteem/main





# [0.34.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.5...@zaaksysteem/main@0.34.0) (2020-06-24)


### Features

* **Object:** MINTY-4239 - Allow objects to be updated and unrelated ([e9f3eb0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e9f3eb0))
* **Object:** MINTY-4239 - Relate created objects to the case ([a73e78e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a73e78e))





## [0.33.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.4...@zaaksysteem/main@0.33.5) (2020-06-23)

**Note:** Version bump only for package @zaaksysteem/main





## [0.33.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.3...@zaaksysteem/main@0.33.4) (2020-06-18)

**Note:** Version bump only for package @zaaksysteem/main





## [0.33.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.2...@zaaksysteem/main@0.33.3) (2020-06-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.33.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.1...@zaaksysteem/main@0.33.2) (2020-06-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.33.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.33.0...@zaaksysteem/main@0.33.1) (2020-06-11)

**Note:** Version bump only for package @zaaksysteem/main





# [0.33.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.32.0...@zaaksysteem/main@0.33.0) (2020-06-11)


### Features

* **Intake:** MINTY-4179 - add document number tooltip ([9cf6c1f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9cf6c1f))





# [0.32.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.31.2...@zaaksysteem/main@0.32.0) (2020-06-09)


### Features

* **Intake:** MINTY-4138 - prefill case create dialog ([fe80a63](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fe80a63))





## [0.31.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.31.1...@zaaksysteem/main@0.31.2) (2020-06-09)


### Bug Fixes

* **Intake:** MINTY-4139 - fix error on sorting by date ([bcae3c4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bcae3c4))





## [0.31.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.31.0...@zaaksysteem/main@0.31.1) (2020-06-08)

**Note:** Version bump only for package @zaaksysteem/main





# [0.31.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.4...@zaaksysteem/main@0.31.0) (2020-06-05)


### Features

* **Case:** Pass object uuid to angular when creating an object ([9c0919e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9c0919e))





## [0.30.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.3...@zaaksysteem/main@0.30.4) (2020-06-05)

**Note:** Version bump only for package @zaaksysteem/main





## [0.30.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.2...@zaaksysteem/main@0.30.3) (2020-06-05)

**Note:** Version bump only for package @zaaksysteem/main





## [0.30.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.1...@zaaksysteem/main@0.30.2) (2020-06-03)

**Note:** Version bump only for package @zaaksysteem/main





## [0.30.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.30.0...@zaaksysteem/main@0.30.1) (2020-05-29)

**Note:** Version bump only for package @zaaksysteem/main





# [0.30.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.29.0...@zaaksysteem/main@0.30.0) (2020-05-28)


### Features

* **case:** MINTY-4034 improve submit button text ([5123187](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5123187))





# [0.29.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.28.3...@zaaksysteem/main@0.29.0) (2020-05-28)


### Features

* **case:** MINTY-4034 allow objects to be created from the case view ([ee6dd74](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ee6dd74))





## [0.28.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.28.2...@zaaksysteem/main@0.28.3) (2020-05-27)

**Note:** Version bump only for package @zaaksysteem/main





## [0.28.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.28.1...@zaaksysteem/main@0.28.2) (2020-05-19)


### Bug Fixes

* **Intake:** MINTY-4020: fix displaying for date field, alignment properties ([b832159](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b832159))





## [0.28.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.28.0...@zaaksysteem/main@0.28.1) (2020-05-14)

**Note:** Version bump only for package @zaaksysteem/main





# [0.28.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.5...@zaaksysteem/main@0.28.0) (2020-05-14)


### Features

* **Intake:** MINTY-3868 - add description column to Intake ([c1c6df2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1c6df2))





## [0.27.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.4...@zaaksysteem/main@0.27.5) (2020-05-13)

**Note:** Version bump only for package @zaaksysteem/main





## [0.27.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.3...@zaaksysteem/main@0.27.4) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.27.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.2...@zaaksysteem/main@0.27.3) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.27.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.1...@zaaksysteem/main@0.27.2) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.27.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.27.0...@zaaksysteem/main@0.27.1) (2020-05-11)

**Note:** Version bump only for package @zaaksysteem/main





# [0.27.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.26.1...@zaaksysteem/main@0.27.0) (2020-05-11)


### Features

* **ObjectView:** MINTY-3809 Fetch Object type and display label, magic_string and description in Object View ([088f049](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/088f049))





## [0.26.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.26.0...@zaaksysteem/main@0.26.1) (2020-05-01)

**Note:** Version bump only for package @zaaksysteem/main





# [0.26.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.25.1...@zaaksysteem/main@0.26.0) (2020-05-01)


### Features

* **CreateObject:** MINTY-3472 - add Rights step to Object Create with associated components ([8699e78](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8699e78))





## [0.25.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.25.0...@zaaksysteem/main@0.25.1) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/main





# [0.25.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.8...@zaaksysteem/main@0.25.0) (2020-04-30)


### Features

* **ObjectView:** MINTY-3779 Implement side menu for object attributes view ([ef41a85](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef41a85))
* **SideMenu:** MINTY-3779 Add SideMenu component ([ec155a4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ec155a4))





## [0.24.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.7...@zaaksysteem/main@0.24.8) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.6...@zaaksysteem/main@0.24.7) (2020-04-28)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.5...@zaaksysteem/main@0.24.6) (2020-04-20)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.4...@zaaksysteem/main@0.24.5) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.3...@zaaksysteem/main@0.24.4) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.2...@zaaksysteem/main@0.24.3) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.1...@zaaksysteem/main@0.24.2) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





## [0.24.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.24.0...@zaaksysteem/main@0.24.1) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/main





# [0.24.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.23.2...@zaaksysteem/main@0.24.0) (2020-04-14)


### Features

* **NavigationBar:** MINTY-3629 Alter main layout to incorporate `NavigationBar` component ([6d73c7d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6d73c7d))





## [0.23.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.23.1...@zaaksysteem/main@0.23.2) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/main





## [0.23.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.23.0...@zaaksysteem/main@0.23.1) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/main





# [0.23.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.22.0...@zaaksysteem/main@0.23.0) (2020-04-09)


### Features

* **BreadcrumbBar:** MINTY-3628 Implement header containing breadcrumb component which can be used in modules ([4a85e72](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a85e72))





# [0.22.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.3...@zaaksysteem/main@0.22.0) (2020-04-09)


### Features

* **TaskItem:** MINTY-3573 - Display details for closed task item ([eff342e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/eff342e))





## [0.21.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.2...@zaaksysteem/main@0.21.3) (2020-04-07)

**Note:** Version bump only for package @zaaksysteem/main





## [0.21.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.1...@zaaksysteem/main@0.21.2) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/main





## [0.21.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.0...@zaaksysteem/main@0.21.1) (2020-04-03)

**Note:** Version bump only for package @zaaksysteem/main





# [0.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.20.0...@zaaksysteem/main@0.21.0) (2020-04-02)


### Features

* **Communication:** MINTY-3432 - add attachment PDF preview ([5bebd92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bebd92))





# [0.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.6...@zaaksysteem/main@0.20.0) (2020-04-01)


### Features

* **ObjectView:** Implement first iteration of Object View ([daa2da5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/daa2da5))





## [0.19.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.5...@zaaksysteem/main@0.19.6) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.4...@zaaksysteem/main@0.19.5) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.3...@zaaksysteem/main@0.19.4) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.2...@zaaksysteem/main@0.19.3) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.1...@zaaksysteem/main@0.19.2) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.0...@zaaksysteem/main@0.19.1) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/main





# [0.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.3...@zaaksysteem/main@0.19.0) (2020-03-30)


### Features

* **FormRenderer:** Improve performance and readability of `FormRenderer` component ([f310425](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f310425))
* **FormRenderer:** Refactor formRenderer to hook and update Formik to latest version 2.1.4 ([c5d1e71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d1e71))





## [0.18.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.2...@zaaksysteem/main@0.18.3) (2020-03-27)

**Note:** Version bump only for package @zaaksysteem/main





## [0.18.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.1...@zaaksysteem/main@0.18.2) (2020-03-20)

**Note:** Version bump only for package @zaaksysteem/main





## [0.18.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.0...@zaaksysteem/main@0.18.1) (2020-03-19)

**Note:** Version bump only for package @zaaksysteem/main





# [0.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.17.2...@zaaksysteem/main@0.18.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - change Loader, exports ([b25d770](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b25d770))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))
* **Intake:** MINTY-3437 - add search component ([2bb171b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2bb171b))





## [0.17.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.17.1...@zaaksysteem/main@0.17.2) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.17.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.17.0...@zaaksysteem/main@0.17.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/main





# [0.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.10...@zaaksysteem/main@0.17.0) (2020-03-05)


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **Communication:** MINTY-3241 Re-enable adding notes and contactmoments on closed cases ([b21dd23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b21dd23))
* **DocumentPreview:** MINTY-2960 Add case document preview to dev dashboard ([0f6fb22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0f6fb22))





## [0.16.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.9...@zaaksysteem/main@0.16.10) (2020-03-03)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.8...@zaaksysteem/main@0.16.9) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.7...@zaaksysteem/main@0.16.8) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.6...@zaaksysteem/main@0.16.7) (2020-02-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.5...@zaaksysteem/main@0.16.6) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.4...@zaaksysteem/main@0.16.5) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.3...@zaaksysteem/main@0.16.4) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.2...@zaaksysteem/main@0.16.3) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.1...@zaaksysteem/main@0.16.2) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/main

## [0.16.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.0...@zaaksysteem/main@0.16.1) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/main

# [0.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.10...@zaaksysteem/main@0.16.0) (2020-02-06)

### Features

- **Communication:** MINTY-2962 Extract Pip form and reuse the same form for create and reply ([42848aa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/42848aa))

## [0.15.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.9...@zaaksysteem/main@0.15.10) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.8...@zaaksysteem/main@0.15.9) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.7...@zaaksysteem/main@0.15.8) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.6...@zaaksysteem/main@0.15.7) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.5...@zaaksysteem/main@0.15.6) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.4...@zaaksysteem/main@0.15.5) (2020-01-22)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.3...@zaaksysteem/main@0.15.4) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.1...@zaaksysteem/main@0.15.3) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.1...@zaaksysteem/main@0.15.2) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.0...@zaaksysteem/main@0.15.1) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/main

# [0.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.14.0...@zaaksysteem/main@0.15.0) (2020-01-10)

### Features

- **Communication:** MINTY-2665 Use `ContactFinder` component to search for email recipients ([c0a4ad6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0a4ad6))

# [0.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.13.0...@zaaksysteem/main@0.14.0) (2020-01-09)

### Bug Fixes

- (CaseManagement) truncate task title ([c5d397b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d397b))
- **Tasks:** MINTY-2769 - fix redirect logic in middleware ([1d09b68](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1d09b68))
- **Tasks:** MINTY-2816 - change get_task_list endpoint to filter ([f127e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f127e36))
- **Tasks:** MINTY-2822 - fix displayname + typing issues ([6f5c722](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6f5c722))

### Features

- **CaseManagement:** add task creator ([939fbbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/939fbbb))
- **ContactFinder:** MINTY-2618 - add type filter support for ContactFinder ([d837b4b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d837b4b))
- **DatePicker:** MINTY-2601 - add onClose ability to DatePicker ([732401e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/732401e))
- **Tasks:** add misc. style/UX improvements ([b619af4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b619af4))
- **Tasks:** Add the basic setup for the task module ([ecc9d4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ecc9d4c))
- **Tasks:** MINTY-1575 - add basic form fields, DatePicker ([07ed5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/07ed5e5))
- **Tasks:** MINTY-1575 - add edit, delete, unlock functionality ([8dca70b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8dca70b))
- **Tasks:** MINTY-1575 - fix build/import problems ([23c0700](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/23c0700))
- **Tasks:** MINTY-2672 - call postmessage for every action ([99bd8fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/99bd8fb))
- **Tasks:** MINTY-2695 - cancel adding of task on blur ([99f96fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/99f96fb))
- **Tasks:** MINTY-2695 - process some MR feedback ([8bd89cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8bd89cc))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [0.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.6...@zaaksysteem/main@0.13.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **Case:** Allow status of case to determine capabilities of communication module ([1a7a781](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1a7a781))
- **Communication:** MINTY-1798 Add `canImportMessage` capability ([c10c88e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c10c88e))
- **Communication:** MINTY-1925 Make deleting a message controllable via a capability ([704ee17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/704ee17))
- **Communication:** MINTY-2034 Call `/get_thread_list` with correct params according to context of communication-module ([7fef0f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7fef0f2))
- **i18n:** Move common locale namespace to `@zaaksysteem/common` package ([4a36e92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a36e92))
- **Message:** Allow source files of messages to be added to the case documents ([6819f60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6819f60))
- **Message:** Make deletion of messages depended on case status ([c1968df](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1968df))
- **Thread:** Allow actions on case-messages to be performed outside of case-context ([2608cbc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2608cbc))
- **Thread:** Allow threads to be linked to cases from main view ([44a8a51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/44a8a51))

## [0.12.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.5...@zaaksysteem/main@0.12.6) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.4...@zaaksysteem/main@0.12.5) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.3...@zaaksysteem/main@0.12.4) (2019-10-29)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.2...@zaaksysteem/main@0.12.3) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.1...@zaaksysteem/main@0.12.2) (2019-10-22)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.0...@zaaksysteem/main@0.12.1) (2019-10-17)

**Note:** Version bump only for package @zaaksysteem/main

# [0.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.3...@zaaksysteem/main@0.12.0) (2019-10-17)

### Features

- **Communciation:** MINTY-1790 Add communication module on `/main/customer-contact` route ([c90005c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c90005c))
- **Communication:** MINTY-1790 Add contacts and customer-contact to main dashboard for testing purposes ([fb27a9a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fb27a9a))
- **Snackbar:** MINTY-1788 Implement snackbar in pip and main app ([dcbe4da](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/dcbe4da))

## [0.11.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.2...@zaaksysteem/main@0.11.3) (2019-09-30)

**Note:** Version bump only for package @zaaksysteem/main

## [0.11.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.1...@zaaksysteem/main@0.11.2) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/main

## [0.11.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.0...@zaaksysteem/main@0.11.1) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/main

# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.10.0...@zaaksysteem/main@0.11.0) (2019-09-09)

### Bug Fixes

- **Typescript:** fix misc. errors ([a5eb790](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a5eb790))

### Features

- **Apps:** Implement typescript ([583155f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/583155f))
- **Note:** Implement typescript ([47808bc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/47808bc))
- **SwitchViewButton:** Implement typescript ([b119c2d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b119c2d))
- **Tabs:** Implement typescript ([7eabf36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7eabf36))
- **Thread:** Implement typescript ([896ada2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/896ada2))
- **ThreadPlaceholder:** Implement typescript ([be532c7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/be532c7))
- **TypeScript:** Add tsconfig.json, which is needed to enable typescript ([da89a6b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/da89a6b))

# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.9.1...@zaaksysteem/main@0.10.0) (2019-09-06)

### Bug Fixes

- **Communication:** restrict rendering contentarea when on threads-only route ([747c4ce](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/747c4ce))

### Features

- **Communication:** MINTY-1289 Add link to case when thread is displayed in the context of a contact ([ed0bd1e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ed0bd1e))
- **Communication:** MINTY-1294 Implement `CaseFinder` component in add `ContactMoment` form ([27af541](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/27af541))
- **Communication:** MINTY-1402 - add support for saving a note ([4fd9e80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4fd9e80))

## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.9.0...@zaaksysteem/main@0.9.1) (2019-09-05)

**Note:** Version bump only for package @zaaksysteem/main

# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.8.1...@zaaksysteem/main@0.9.0) (2019-08-29)

### Bug Fixes

- **Communication:** fix invalid value when saving a contact ([4ea871b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4ea871b))
- **Communication:** MINTY-1386: fix uuid of contact when saving ([771f520](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/771f520))
- **Communication:** MINTY-1391 - display thread content with newlines ([f754156](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f754156))
- **Communication:** Update thread unwrapping to the fixed API ([3604122](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3604122))

### Features

- **Communication:** add person/company icons to Contactfinder, some styling ([a2f2da2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2f2da2))
- **Communication:** Add thread views for contactmoment and note ([e60867e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e60867e))
- **Communication:** Add view for notes ([4481b91](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4481b91))
- **Communication:** change breakpoints so medium will show thread list ([1239fba](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1239fba))
- **Communication:** Implement API v2 for threadview ([c5f2689](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5f2689))
- **Communication:** Implement placeholder for the view side ([4271423](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4271423))
- **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
- **Communication:** MINTY-1115 Add `Threads` filter component ([409738f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/409738f))
- **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
- **Communication:** MINTY-1115 Move add and refresh button to `Threads` list ([e806749](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e806749))
- **Communication:** MINTY-1126 Style add button ([594ab7a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/594ab7a))
- **Communication:** MINTY-1260 - save Contactmoment, misc. styling and refactoring ([1c1b872](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1c1b872))
- **Communication:** MINTY-1289 Add communication module to contact route ([5ab6ba4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ab6ba4))
- **Communication:** Process MR feedback ([8b17f80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b17f80))
- **Communication:** tweak some misc. styling ([e1a16b5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1a16b5))
- **Communication:** Tweak some styling ([bb1158d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bb1158d))
- **ErrorHandling:** MINTY-1126 Centralized error handling displaying a `Alert` for every failed async action ([d65d459](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d65d459))
- **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))

## [0.8.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.8.0...@zaaksysteem/main@0.8.1) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/main

# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.2...@zaaksysteem/main@0.8.0) (2019-08-22)

### Features

- **apps:** Prevent automatic opening of browser tabs on start ([4740421](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4740421))

## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.1...@zaaksysteem/main@0.7.2) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/main

## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.0...@zaaksysteem/main@0.7.1) (2019-08-06)

**Note:** Version bump only for package @zaaksysteem/main

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.6.0...@zaaksysteem/main@0.7.0) (2019-08-01)

### Features

- **Iframe:** MINTY-1121 When running in an iframe, notify top window of location changes ([4618672](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4618672))

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.5.0...@zaaksysteem/main@0.6.0) (2019-07-31)

### Features

- **Communication:** MINTY-1126 Add case reducer which provides information needed for communication module & add temporary dashboard, useful for navigating to open cases ([6d70c7d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6d70c7d))

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.4.1...@zaaksysteem/main@0.5.0) (2019-07-31)

### Features

- **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))

## [0.4.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.4.0...@zaaksysteem/main@0.4.1) (2019-07-30)

**Note:** Version bump only for package @zaaksysteem/main

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.3.0...@zaaksysteem/main@0.4.0) (2019-07-29)

### Features

- **Dates:** MINTY-1120 Add localized date formatting lib `fecha` ([84b6693](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/84b6693))

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.2.0...@zaaksysteem/main@0.3.0) (2019-07-29)

### Features

- **i18n:** MINTY-1120 Add i18next to setup and define APP_CONTEXT_ROOT variable on build time. ([a3e6a4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a3e6a4c))

# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.1.0...@zaaksysteem/main@0.2.0) (2019-07-25)

### Features

- **main:** MINTY-1237 Add session state to @zaaksysteem/common and implement in app ([575898f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/575898f))

# 0.1.0 (2019-07-25)

### Features

- **main:** MINTY-1232 Add `connected-react-router` package to link router to redux store ([a673309](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a673309))

## [0.1.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/compare/@zaaksysteem/case-management@0.1.0...@zaaksysteem/case-management@0.1.1) (2019-07-23)

**Note:** Version bump only for package @zaaksysteem/case-management

# 0.1.0 (2019-07-18)

### Features

- **CI:** MINTY-1120 Add gitlab CI file ([f4e971e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/f4e971e))
- **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
- **Setup:** MINTY-1120 Add eslint and prettier ([0e6b94c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/0e6b94c))
- **Setup:** MINTY-1120 Add storybook ([1dc9406](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/1dc9406))
- **Setup:** MINTY-1120 Centralize create react app overrides in common package ([3cdc9d7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/3cdc9d7))
- **Setup:** MINTY-1120 Setup monorepo for all apps and packges using lerna and yarn workspaces ([6bd626e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/6bd626e))
