// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type ActionDataAllocationType = { [key: string]: any };
export type ActionRelationTypeType =
  | 'deelzaak'
  | 'gerelateerd'
  | 'vervolgzaak_datum'
  | 'vervolgzaak';
type ActionDataCaseType = {
  relatie_type: ActionRelationTypeType;
  [key: string]: any;
};
type ActionDataEmailType = {
  rcpt: 'behandelaar' | 'aanvrager' | 'gemachtigde' | 'betrokkene' | 'overig';
  email: string;
  cc: string;
  bcc: string;
  subject: string;
  body: string;
  [key: string]: any;
};
type ActionDataObjectActionType = { [key: string]: any };
type ActionDataSubjectActionType = {
  betrokkene_type: 'natuurlijk_persoon' | 'bedrijf';
  [key: string]: any;
};
type ActionDataTemplateType = {
  target_format: string;
  [key: string]: any;
};
type ActionDataXentialType = {
  target_format: string;
  interface_id: number;
  [key: string]: any;
};

type ActionDataType =
  | ActionDataAllocationType
  | ActionDataCaseType
  | ActionDataEmailType
  | ActionDataObjectActionType
  | ActionDataSubjectActionType
  | ActionDataTemplateType
  | ActionDataXentialType;

type ActionTypeType =
  | 'allocation'
  | 'case'
  | 'email'
  | 'object_action'
  | 'subject'
  | 'template';

export type ActionType = {
  id: number;
  automatic: boolean;
  description: string;
  label: string;
  tainted: boolean;
  type: ActionTypeType;
  data: ActionDataType;
  icon: React.ComponentType;
};

export type SaveActionTypeType =
  | 'toggleCheck'
  | 'untaint'
  | 'commit'
  | 'execute';

export type SaveActionType = (
  type: SaveActionTypeType,
  action: ActionType
) => void;

export type UpdateActionType = (
  caseNumber: number,
  type: SaveActionTypeType,
  action: ActionType
) => Promise<ActionType | null>;

export type MergeActionType = (
  actions: ActionType[],
  actionToMerge: ActionType
) => ActionType[];
