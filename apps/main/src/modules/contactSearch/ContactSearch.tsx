// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import TopBar from '../../components/TopBar/TopBar';
import { PanelLayout } from '../../components/PanelLayout/PanelLayout';
import { Panel } from '../../components/PanelLayout/Panel';
import { useStyles } from './ContactSearch.styles';
import { ContactSearchSideMenu } from './ContactSearchSideMenu';
import { ContactType, ResultType, SearchType } from './ContactSearch.types';
import { performSearch } from './ContactSearch.library';
import SearchForm from './components/SearchForm';
import ResultsTable from './components/ResultsTable';

export interface ContactSearchPropsType {}

export type ContactSearchParamsType = {
  type: ContactType;
};

/* eslint complexity: [2, 8] */
const ContactSearch: React.FunctionComponent<ContactSearchPropsType> = () => {
  const { type } = useParams<
    keyof ContactSearchParamsType
  >() as ContactSearchParamsType;
  const classes = useStyles();
  const [t] = useTranslation('contactSearch');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [results, setResults] = useState<ResultType[]>();

  useEffect(() => {
    setResults(undefined);
  }, [type]);

  const search: SearchType = values => {
    performSearch(type, values)
      .then((results: ResultType[]) => {
        setResults(results);
      })
      .catch(openServerErrorDialog);
  };

  return (
    <div className={classes.wrapper}>
      <TopBar title={t('contactSearch')} />
      <div className={classes.content}>
        <PanelLayout>
          <Panel type="side">
            <ContactSearchSideMenu type={type} />
          </Panel>
          <Panel className={classes.view}>
            {results && (
              <ResultsTable
                type={type}
                results={results}
                setResults={setResults}
              />
            )}

            {/*
              when navigating to a different type,
              this will render a new form
              with the formDefinition of the other type
            */}

            {!results && type === 'person' && (
              <SearchForm type={type} search={search} />
            )}
            {!results && type === 'organization' && (
              <SearchForm type={type} search={search} />
            )}
            {!results && type === 'employee' && (
              <SearchForm type={type} search={search} />
            )}
          </Panel>
        </PanelLayout>
      </div>
      {ServerErrorDialog}
    </div>
  );
};

export default ContactSearch;
