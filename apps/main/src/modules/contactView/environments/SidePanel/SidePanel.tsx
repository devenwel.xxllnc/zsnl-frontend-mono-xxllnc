// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import {
  ControlPanelType,
  EnvironmentType,
  HostType,
  UpdateHostsType,
} from '../Environments.types';
import { useStyles } from './SidePanel.styles';
import {
  getDiskSpace,
  getEnvironmentCounter,
  getUptime,
} from './SidePanel.library';
import Hosts from './Hosts';
import Overview from './Overview';

type SidePanelPropsType = {
  controlPanel: ControlPanelType;
  environments: EnvironmentType[];
  hosts: HostType[];
  updateHosts: UpdateHostsType;
};

const SidePanel: React.FunctionComponent<SidePanelPropsType> = ({
  controlPanel,
  environments,
  hosts,
  updateHosts,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('environments');

  return (
    <div className={classes.wrapper}>
      <Typography variant="h5">{t('overview.title')}</Typography>
      <Overview controlPanel={controlPanel} />

      <Typography variant="h5">{t('hosts.title')}</Typography>
      <div>
        <Hosts
          controlPanel={controlPanel}
          environments={environments}
          hosts={hosts}
          updateHosts={updateHosts}
        />
      </div>

      <Typography variant="h5">{t('diskspace.title')}</Typography>
      <div>{getDiskSpace(controlPanel, environments)}</div>

      <Typography variant="h5">{t('environmentsCount.title')}</Typography>
      <div>{getEnvironmentCounter(controlPanel, environments)}</div>

      <Typography variant="h5">{t('uptime.title')}</Typography>
      <div>{getUptime(environments)}</div>
    </div>
  );
};

export default SidePanel;
