// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { ControlPanelType } from '../Environments.types';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  controlPanel: ControlPanelType
) => AnyFormDefinitionField[];

export const getOverviewDialogFormDefinition: GetFormDefinitionType = (
  t,
  controlPanel
) => {
  const formDefinition = [
    {
      name: 'template',
      type: fieldTypes.SELECT,
      value: controlPanel.template,
      required: true,
      isClearable: false,
      label: t('overview.dialog.labels.template'),
      choices: controlPanel.availableTemplates.map(template => ({
        label: template,
        value: template,
      })),
    },
    {
      name: 'allowedEnvironments',
      type: fieldTypes.TEXT,
      value: controlPanel.allowedEnvironments || '',
      required: true,
      label: t('overview.dialog.labels.allowedEnvironments'),
    },
    {
      name: 'allowedDiskspace',
      type: fieldTypes.TEXT,
      value: controlPanel.allowedDiskspace || '',
      required: true,
      label: t('overview.dialog.labels.allowedDiskspace'),
    },
    {
      name: 'readOnly',
      type: fieldTypes.CHECKBOX,
      value: true,
      label: t('overview.dialog.labels.readOnly'),
      suppressLabel: true,
    },
  ];

  return formDefinition;
};
