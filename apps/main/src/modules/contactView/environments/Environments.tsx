// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar.cmp';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { PanelLayout } from '../../../components/PanelLayout/PanelLayout';
import { Panel } from '../../../components/PanelLayout/Panel';
import {
  IdType,
  ControlPanelType,
  EnvironmentType,
  HostType,
  UpdateEnvironmentsType,
  UpdateHostsType,
} from './Environments.types';
import {
  getId,
  getEnvironments,
  getHosts,
  getControlPanels,
  saveEnvironmentAction,
  saveHostAction,
  mergeHosts,
  mergeEnvironments,
} from './Environments.library';
import Setup from './Setup/Setup';
import EnvironmentsTable from './EnvironmentsTable/EnvironmentsTable';
import SidePanel from './SidePanel/SidePanel';
import { useStyles } from './Environments.styles';

export type EnvironmentsPropsType = {
  uuid: string;
};

/* eslint complexity: [2, 7] */
const Environments: React.FunctionComponent<EnvironmentsPropsType> = ({
  uuid,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('environments');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [snack, setSnack] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [id, setId] = useState<IdType>();
  const [controlPanel, setControlPanel] = useState<ControlPanelType>();
  const [environments, setEnvironments] = useState<EnvironmentType[]>();
  const [hosts, setHosts] = useState<HostType[]>();

  useEffect(() => {
    if (!id) {
      getId(uuid, setId);
    }

    if (id && !controlPanel) {
      getControlPanels(id).then((controlPanel: ControlPanelType) => {
        setControlPanel(controlPanel);
        setLoading(false);
      });
    }

    if (controlPanel && controlPanel.uuid) {
      getEnvironments(controlPanel.uuid, setEnvironments);
      getHosts(controlPanel.uuid, setHosts);
    }
  }, [id, controlPanel]);

  if (loading || !id || !controlPanel) return <Loader />;

  const refreshControlPanel = () => {
    getControlPanels(id).then((controlPanel: ControlPanelType) => {
      setControlPanel(controlPanel);
      setLoading(false);
    });
  };

  const refreshEnvironments = () => {
    getEnvironments(controlPanel.uuid, setEnvironments);
  };

  if (!controlPanel.uuid) {
    return (
      <Setup
        id={id}
        setLoading={setLoading}
        refreshControlPanel={refreshControlPanel}
      />
    );
  }

  if (!environments || !hosts) {
    return <Loader />;
  }

  const updateEnvironments: UpdateEnvironmentsType = (
    actionType,
    values,
    environment,
    onclose
  ) => {
    saveEnvironmentAction(controlPanel.uuid, values, environment?.uuid)
      .then(newEnvironment => {
        const newEnvironments = mergeEnvironments(
          actionType,
          environments,
          newEnvironment
        );

        const actionVerb =
          values.protected || values.active
            ? t('common:verbs.activated')
            : t('common:verbs.deactivated');

        setEnvironments(newEnvironments);
        setSnack(
          t(`table.snacks.${actionType}`, { verb: actionVerb.toLowerCase() })
        );

        if (onclose) {
          onclose();
        }
      })
      .catch(openServerErrorDialog);
  };

  const updateHosts: UpdateHostsType = (actionType, values, host, onClose) => {
    saveHostAction(controlPanel.uuid, actionType, values, host?.uuid)
      .then(newHost => {
        const newHosts = mergeHosts(actionType, hosts, newHost);

        // we need to refresh the environments,
        // because it holds the information for which environment a host is assigned to
        refreshEnvironments();
        setHosts(newHosts);
        setSnack(t(`hosts.snacks.${actionType}`));
        onClose();
      })
      .catch(openServerErrorDialog);
  };

  const sortedHosts = hosts.sort((hostA, hostB) =>
    hostA.label < hostB.label ? -1 : 1
  );

  return (
    <div className={classes.wrapper}>
      <PanelLayout>
        <Panel className={classes.content}>
          <EnvironmentsTable
            controlPanel={controlPanel}
            environments={environments}
            updateEnvironments={updateEnvironments}
          />
        </Panel>
        <Panel className={classes.sidePanel} type="side">
          <SidePanel
            controlPanel={controlPanel}
            environments={environments}
            hosts={sortedHosts}
            updateHosts={updateHosts}
          />
        </Panel>
        <Snackbar
          autoHideDuration={5000}
          handleClose={(event: React.ChangeEvent, reason: string) => {
            if (reason === 'clickaway') return;
            setSnack(null);
          }}
          message={snack}
          open={Boolean(snack)}
        />
      </PanelLayout>
      {ServerErrorDialog}
    </div>
  );
};

export default Environments;
