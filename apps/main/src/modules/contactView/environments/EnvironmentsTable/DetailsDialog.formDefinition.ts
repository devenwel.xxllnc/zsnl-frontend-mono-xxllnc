// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { EnvironmentType } from '../Environments.types';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  environment: EnvironmentType
) => AnyFormDefinitionField[];

export const getDetailsDialogFormDefinition: GetFormDefinitionType = (
  t,
  environment
) => {
  const formDefinition = [
    'uuid',
    'fqdn',
    'customerType',
    'softwareVersion',
    'fallbackUrl',
    'provisionedOn',
    'servicesDomain',
    'apiDomain',
    'database',
    'databaseHost',
    'filestore',
    'freeformReference',
    'diskspace',
  ].map(fieldName => {
    let value =
      // @ts-ignore
      environment[fieldName] === null ? '' : `${environment[fieldName]}`; // stringify numbers

    if (fieldName === 'customerType') {
      value = t(`customerTypes.${value}`);
    }

    if (fieldName === 'softwareVersion') {
      value = t(`softwareVersions.${value}`);
    }

    return {
      name: fieldName,
      label: t(`table.details.labels.${fieldName}`),
      type: fieldTypes.TEXT,
      readOnly: true,
      value,
    };
  });

  return formDefinition;
};
