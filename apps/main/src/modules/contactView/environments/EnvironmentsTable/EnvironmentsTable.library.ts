// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  CustomerTypeType,
  OtapType,
  SoftwareVersionType,
} from './../Environments.types';

export const customerTypes: CustomerTypeType[] = [
  'commercial',
  'government',
  'lab',
  'development',
  'staging',
  'acceptance',
  'preprod',
  'testing',
  'production',
];

export const otapTypes: OtapType[] = [
  'development',
  'testing',
  'accept',
  'production',
];

export const softwareVersions: SoftwareVersionType[] = ['master'];

const columns = [
  {
    name: 'status',
    width: 100,
  },
  { name: 'label', width: 200 },
  {
    name: 'webAddress',
    width: 1,
    flexGrow: 1,
  },
  {
    name: 'hosts',
    width: 1,
    flexGrow: 1,
  },
  {
    name: 'template',
    width: 150,
  },
  {
    name: 'otap',
    width: 120,
  },
];

const actionIconWidth = 47;
const cellMargins = 20;
const actionsCellWidth = actionIconWidth * 4 + cellMargins;

export const getColumns = (t: i18next.TFunction, cellRenderer: any) => [
  ...columns.map(column => ({
    ...column,
    minWidth: 100,
    label: t(`table.${column.name}.header`),
    cellRenderer,
    disableSort: true,
  })),
  {
    name: 'actions',
    width: actionsCellWidth,
    minWidth: actionsCellWidth,
    cellRenderer,
    disableSort: true,
  },
];
