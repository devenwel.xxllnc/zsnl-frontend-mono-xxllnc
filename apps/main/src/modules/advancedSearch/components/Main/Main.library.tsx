// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import * as i18next from 'i18next';
import { ModeType } from '../../AdvancedSearch.types';

export const getLabel = ({
  mode,
  currentQuery,
  t,
}: {
  mode: ModeType;
  currentQuery: any;
  t: i18next.TFunction;
}) => (mode === 'new' ? t('new') : currentQuery?.data?.name || '');
