// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { DateEntryType } from '../../../../AdvancedSearch.types';

export type DateTimeDurationValueType = string | 'now';
export type KindType = 'ISO' | 'duration' | 'now' | null;
export type PeriodType = '-' | '+';
export type IntervalType =
  | 'years'
  | 'months'
  | 'days'
  | 'hours'
  | 'minutes'
  | 'seconds';

export type DialogPropertiesType = {
  open: boolean;
  index: number | null;
  entry: DateEntryType | null;
};
