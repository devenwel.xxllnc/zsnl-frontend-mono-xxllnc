// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { ColumnType, KindType } from '../AdvancedSearch.types';

export const SAVED_SEARCHES_PAGE_LENGTH = 20;
export const DEFAULT_RESULTS_PER_PAGE = 25;

export const defaultOptions = {
  queries: {
    staleTime: Infinity,
    refetchOnWindowFocus: false,
    retry: 0,
  },
};

export const getCategories = (kind: KindType) =>
  kind === 'custom_object'
    ? {
        default: [
          'uuid',
          'title',
          'subtitle',
          'externalReference',
          'dateCreated',
          'lastModified',
        ],
      }
    : {
        systemAttributes: ['id', 'title'],
        requestor: ['aap', 'noot'],
        assignee: ['mies'],
        coordinator: ['wim'],
        correspondence: ['bok'],
        dates: ['zus'],
        result: ['vuur'],
        case: ['lex'],
        other: ['gijs', 'kees'],
      };

export const getDefaultColumns = (
  kind: KindType,
  t: i18next.TFunction
): ColumnType[] => {
  let columns: Array<string | Array<string>>[];

  if (kind === 'custom_object') {
    columns = [
      ['uuid', t('columns.id'), ['id']],
      ['title', t('columns.title'), ['attributes', 'title']],
      ['subtitle', t('columns.subtitle'), ['attributes', 'subtitle']],
      [
        'externalReference',
        t('columns.externalReference'),
        ['attributes', 'external_reference'],
      ],
      ['dateCreated', t('columns.dateCreated'), ['attributes', 'date_created']],
      [
        'lastModified',
        t('columns.lastModified'),
        ['attributes', 'last_modified'],
      ],
    ];
  } else {
    columns = [
      ['id', 'id', ['id']],
      ['title', 'title', ['title']],
      ['aap', 'aap', ['aap']],
      ['noot', 'noot', ['noot']],
    ];
  }
  return columns.map(entry => ({
    type: entry[0] as string,
    label: entry[1] as string,
    source: entry[2] as Array<string>,
  }));
};

export const SORTABLE_COLUMNS = [
  ['attributes', 'title'],
  ['attributes', 'subtitle'],
  ['attributes', 'external_reference'],
  ['attributes', 'date_created'],
  ['attributes', 'last_modified'],
];

export const filterTranslationKeys = {
  'attributes.last_modified': 'modified',
  'attributes.status': 'status',
  'attributes.archive_status': 'archiveStatus',
  'attributes.archival_state': 'archivalState',
};

export const filtersKeyNamesReplacements = [
  ['startValue', 'start_value'],
  ['endValue', 'end_value'],
  ['timeSetByUser', 'time_set_by_user'],
  ['magicString', 'magic_string'],
  ['customFieldsOperator', 'custom_fields_operator'],
];
