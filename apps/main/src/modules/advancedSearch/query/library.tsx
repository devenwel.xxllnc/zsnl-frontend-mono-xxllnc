// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import fecha from 'fecha';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import Button from '@mintlab/ui/App/Material/Button';
import { QueryClient } from '@tanstack/react-query';
import {
  QUERY_KEY_SAVEDSEARCHES,
  QUERY_KEY_SAVEDSEARCH,
  QUERY_KEY_RESULTS,
  QUERY_KEY_RESULTS_COUNT,
  QUERY_KEY_CUSTOM_FIELDS,
} from './constants';

const DATE_FORMAT = 'DD-MM-YYYY';

export const invalidateAllQueries = (client: QueryClient) => {
  client.invalidateQueries([QUERY_KEY_SAVEDSEARCHES]);
  client.invalidateQueries([QUERY_KEY_SAVEDSEARCH]);
  client.invalidateQueries([QUERY_KEY_RESULTS]);
  client.invalidateQueries([QUERY_KEY_RESULTS_COUNT]);
  client.invalidateQueries([QUERY_KEY_CUSTOM_FIELDS]);
};

type LinkButtonPropsType = {
  title: string;
  url: string;
  t: i18next.TFunction;
};
export const LinkButton: FunctionComponent<LinkButtonPropsType> = ({
  title,
  url,
  t,
}) => (
  <Button
    name="openInNewWindow"
    title={t('results.openInNew')}
    action={(event: any) => {
      event.stopPropagation();
      window.open(url, '_blank');
    }}
    endIcon={<Icon size="extraSmall">{iconNames.open_in_new}</Icon>}
    variant={'text'}
    sx={{
      color: ({ palette: { primary } }: any) => primary.main,
    }}
  >
    {title}
  </Button>
);

/* eslint complexity: [2, 20] */
/**
 * Currently supports rendering of these attribute types:
Geo
Relatie (object + subject)
Rekeningnummer
Datum
E-mail
Geocoordinaten (lat/lon)
Numeriek
Enkelvoudige keuze
Keuzelijst
Tekstveld
Groot tekstveld
Webadres
Valuta
 */

export const resultValueObjToColumnValue = (
  resultValue: any,
  row: APICaseManagement.SearchCustomObjectsResponseBody['data'][0],
  t: i18next.TFunction
) => {
  const actualValue = () => {
    return resultValue?.type && resultValue?.value
      ? resultValue.value
      : resultValue;
  };
  const safeValue = (value: any) => `${value}`;
  const type = resultValue?.type;
  //const isDate = (value: any) => Boolean(fecha.parse(value, DATE_FORMAT));
  const isDate = (value: any) => false;
  const formatDate = (value: any) => {
    const dateObj = new Date(value);
    return fecha.format(dateObj, DATE_FORMAT);
  };

  const getTypeValue = () => {
    switch (type) {
      case 'text': {
        return safeValue(actualValue());
      }
      case 'email': {
        return (
          <LinkButton
            title={t('results.types.email')}
            url={`mailto:${actualValue()}`}
            t={t}
          />
        );
      }
      case 'date': {
        return formatDate(actualValue());
      }
      case 'geojson': {
        return (
          <LinkButton
            title={t('results.types.geojson')}
            url={`/main/object/${row?.id}/map`}
            t={t}
          />
        );
      }
      case 'url': {
        return (
          <LinkButton
            title={t('results.types.url')}
            url={actualValue()}
            t={t}
          />
        );
      }
      case 'address_v2': {
        return actualValue()?.address?.full || '';
      }
      case 'currency': {
        return new Intl.NumberFormat('nl-NL', {
          style: 'currency',
          currency: 'EUR',
        }).format(actualValue());
      }
      case 'relationship': {
        const getProps = () => {
          switch (resultValue.specifics.relationship_type) {
            case 'custom_object':
              return {
                url: `/main/object/${actualValue()}/`,
                title: t('results.types.relationshipTypes.customObject'),
              };
            case 'subject':
              return {
                url: `/redirect/contact_page?uuid=${actualValue()}`,
                title: t('results.types.relationshipTypes.subject'),
              };
            default:
              return {
                url: '/',
                title: t('results.types.relationshipTypes.unknown'),
              };
          }
        };
        const props = getProps();
        return <LinkButton title={props.title} url={props.url} t={t} />;
      }
      default: {
        return safeValue(actualValue());
      }
    }
  };

  if (resultValue === null || resultValue === undefined) {
    return '';
  } else if (type) {
    return getTypeValue();
  } else if (isDate(actualValue())) {
    return formatDate(actualValue());
  } else if (typeof resultValue === 'string' || resultValue instanceof String) {
    return resultValue;
  } else {
    return safeValue(actualValue());
  }
};
