// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { CaseObjType } from '../case/Case.types';
import Tasks from '../case/views/phases/Tasks';
import { TasksPropsType } from '../case/views/phases/Tasks/Tasks';

type TasksWrapperPropsType = Omit<
  TasksPropsType,
  'canEditSideBar' | 'caseUuid'
> & { caseObj: CaseObjType };

type TasksParamsType = {
  phaseNumber: string;
};

const TasksWrapper: React.ComponentType<TasksWrapperPropsType> = props => {
  const caseObj = props.caseObj;
  const { phaseNumber } = useParams<keyof TasksParamsType>() as TasksParamsType;

  const canEditSideBar =
    caseObj.canEdit && Number(phaseNumber) >= caseObj.phase;

  return (
    <Tasks {...props} caseUuid={caseObj.uuid} canEditSideBar={canEditSideBar} />
  );
};

export default TasksWrapper;
