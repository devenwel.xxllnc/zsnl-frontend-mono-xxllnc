// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useNavigationBarStyles = makeStyles((theme: Theme) => {
  const {
    palette: { lizard, cloud },
    mintlab: { greyscale },
  } = theme;

  return {
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
      height: '100%',
      overflowX: 'hidden',
      overflowY: 'hidden',
      padding: '10px 0',
      width: 70,
      backgroundColor: lizard.dark,
      whiteSpace: 'nowrap',
    },
    open: {
      width: 260,
      transition: theme.transitions.create(
        // @ts-ignore
        ['background-color', 'width'],
        {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }
      ),
      backgroundColor: cloud.light,
    },
    close: {
      // @ts-ignore
      transition: theme.transitions.create(['background-color', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    topWrapper: {
      flex: 1,
    },
    bottomWrapper: {
      flex: 0,
    },
    menuButtonWrapper: {
      width: 70,
      textAlign: 'center',
      color: greyscale.darkest,
    },
    list: {
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    separator: {
      height: 30,
      position: 'relative',
      '&::after': {
        content: '""',
        position: 'absolute',
        width: '100%',
        height: 1,
        backgroundColor: greyscale.darkest,
        top: 15,
      },
    },
    icon: {
      paddingLeft: 5,
    },
    textLink: {
      fontSize: 12,
      fontWeight: 500,
      color: greyscale.darkest,
    },
  };
});
