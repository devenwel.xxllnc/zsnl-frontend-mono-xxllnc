// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { useBreadcrumbsStyles } from './Breadcrumbs.style';

export type BreadcrumbsPropsType = {
  title?: string;
  subTitle?: string;
};

const Breadcrumbs: React.ComponentType<BreadcrumbsPropsType> = ({
  title,
  subTitle,
}) => {
  const [t] = useTranslation();
  const classes = useBreadcrumbsStyles();
  const dashboardTitle = t('main:modules.dashboard');

  return (
    <div className={classes.wrapper}>
      {title ? (
        <>
          <a className={classes.dashboardLink} href="/intern">
            {dashboardTitle}
          </a>
          <Icon
            size="small"
            classes={{
              root: classes.separator,
            }}
          >
            {iconNames.navigate_next}
          </Icon>
          <div className={classes.title}>{title}</div>
          {subTitle && (
            <Tooltip title={subTitle} className={classes.subTitleWrapper}>
              <div className={classes.subTitle}>{subTitle}</div>
            </Tooltip>
          )}
        </>
      ) : (
        <div>{dashboardTitle}</div>
      )}
    </div>
  );
};

export default Breadcrumbs;
