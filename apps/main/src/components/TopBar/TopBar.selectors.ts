// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SessionRootStateType } from '@zaaksysteem/common/src/store/session/session.reducer';

export const contactSelector = (state: any) => {
  const parts = state.router.location.pathname.split('/');
  if (parts.some((part: string) => part === 'contact-view')) {
    return {
      uuid: parts.filter((part: string) => part.length === 36)[0],
      type: parts.filter((part: string) =>
        ['employee', 'organization', 'person'].includes(part)
      )[0],
    };
  }
  return null;
};

export const capabilitiesSelector = (state: SessionRootStateType) =>
  state.session?.data?.logged_in_user?.capabilities || [];
