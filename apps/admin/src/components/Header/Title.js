// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material';

export const Title = ({ children }) => {
  const theme = useTheme();

  return (
    <Typography
      variant="h3"
      sx={{
        fontWeight: theme.typography.fontWeightMedium,
        fontSize: '1.5625rem',
      }}
    >
      {children}
    </Typography>
  );
};

export default Title;
