// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import { subAppHeaderStylesheet } from './SubAppHeader.style';

/**
 * @reactProps {*} children
 * @reactProps {Object} classes
 * @return {ReactElement}
 */
export const SubAppHeader = ({ children, classes }) => (
  <header className={classes.header}>{children}</header>
);

export default withStyles(subAppHeaderStylesheet)(SubAppHeader);
