// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const exceptionStyleSheet = ({ palette: { error, review } }) => ({
  error: {
    boxSizing: 'border-box',
    width: '100%',
    minHeight: '99%',
    border: `1px solid ${error.dark}`,
    outline: 'none',
    margin: 0,
    padding: '1rem',
    backgroundColor: review.light,

    '&::selection': {
      color: '#fff',
      background: error.dark,
    },
  },
});
