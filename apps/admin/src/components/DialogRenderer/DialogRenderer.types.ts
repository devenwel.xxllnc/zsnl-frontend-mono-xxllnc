// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';

export type DialogPropsType = {
  hide: () => void;
  invoke: (options: { path: string; force?: boolean }) => void;
  t: i18next.TFunction;
  options: any;
};
