// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const separatorMargin = '8px';
const separatorSize = '4px';

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const cellStyleSheet = ({
  mintlab: { greyscale },
  palette: {
    common: { black },
  },
}) => ({
  dateTime: {
    overflow: 'hidden',
  },
  dateTimeWrapper: {
    whiteSpace: 'nowrap',
    display: 'inline-block',
  },
  date: {
    marginRight: `calc(${separatorMargin} * 2 + ${separatorSize})`,
  },
  time: {
    color: greyscale.evenDarker,
    marginLeft: `calc(0px - ${separatorMargin} - ${separatorSize})`,
    '&::before': {
      content: '"·"',
      fontWeight: 'bold',
      marginRight: separatorMargin,
    },
  },
  link: {
    textDecoration: 'none',
    color: black,
    '&:hover': {
      textDecoration: 'underline',
    },
  },
});
