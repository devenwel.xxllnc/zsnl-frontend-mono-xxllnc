// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const ROUTE_INVOKE = 'ROUTE:INVOKE';
export const ROUTE_RESOLVE = 'ROUTE:RESOLVE';
