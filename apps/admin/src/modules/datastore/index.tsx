// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import DataStore from './DataStore';
import locale from './DataStore.locale';
import { defaultOptions } from './DataStore.config';

type DatastoreRouteType = {};

const DataStoreModule: React.ComponentType<DatastoreRouteType> = () => {
  const queryClient = new QueryClient({ defaultOptions });

  return (
    <QueryClientProvider client={queryClient}>
      <I18nResourceBundle resource={locale} namespace="dataStore">
        <DataStore queryClient={queryClient} />
      </I18nResourceBundle>
    </QueryClientProvider>
  );
};

export default DataStoreModule;
