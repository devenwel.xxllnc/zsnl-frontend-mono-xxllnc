// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { QueryClient, useQuery } from '@tanstack/react-query';
import { useSelectionBehaviour } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import Typography from '@mui/material/Typography';
import SubAppHeader from '../../components/Header/SubAppHeader';
import { useDataStoreStyles } from './DataStore.style';
import { DataTypeType, FiltersType } from './DataStore.types';
import { defaultDataType, defaultFilters, getData } from './DataStore.library';
import ActionBar from './ActionBar/ActionBar';
import DataTable from './DataTable/DataTable';

type DataStorePropsType = {
  queryClient: QueryClient;
};

const DataStore: React.ComponentType<DataStorePropsType> = ({
  queryClient,
}) => {
  const classes = useDataStoreStyles();
  const [t] = useTranslation('dataStore');
  const [dataType, setDataType] = useState<DataTypeType>(defaultDataType);
  const [filters, setFilters] = useState<FiltersType>(defaultFilters);

  const selectionProps = useSelectionBehaviour({
    selectAllTranslations: t('table.selectAll', {
      returnObjects: true,
    }),
    page: filters.zapi_page,
    resultsPerPage: filters.zapi_num_rows,
  });

  const { data } = useQuery(
    ['data', dataType, filters],
    () => getData(dataType, filters),
    {
      onSuccess: () => selectionProps.resetAll(),
    }
  );

  const refreshData = () => queryClient.invalidateQueries(['data']);
  const { selected, allSelected } = selectionProps;

  return (
    <div className={classes.wrapper}>
      <SubAppHeader>
        <Typography variant="h3">{t('dataStore')}</Typography>
      </SubAppHeader>
      <ActionBar
        data={data}
        dataType={dataType}
        filters={filters}
        selected={selected}
        allSelected={allSelected}
        setDataType={setDataType}
        setFilters={setFilters}
      />
      <DataTable
        data={data}
        dataType={dataType}
        filters={filters}
        setFilters={setFilters}
        refreshData={refreshData}
        selectionProps={selectionProps}
      />
    </div>
  );
};

export default DataStore;
