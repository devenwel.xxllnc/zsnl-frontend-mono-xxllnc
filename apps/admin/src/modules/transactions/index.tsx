// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Transactions from './Transactions/Transactions';
import Transaction from './Transaction/Transaction';
import locale from './Transactions.locale';
import { defaultOptions } from './Transactions.config';

type TransactionsModuleType = {};

const TransactionsModule: React.ComponentType<TransactionsModuleType> = () => {
  const queryClient = new QueryClient({ defaultOptions });

  return (
    <QueryClientProvider client={queryClient}>
      <I18nResourceBundle resource={locale} namespace="transactions">
        <Router>
          <Routes>
            <Route path={'admin/transacties'}>
              <Route
                path={''}
                element={<Transactions queryClient={queryClient} />}
              />
              <Route path={':transactionUuid'} element={<Transaction />} />
            </Route>
          </Routes>
        </Router>
      </I18nResourceBundle>
    </QueryClientProvider>
  );
};

export default TransactionsModule;
