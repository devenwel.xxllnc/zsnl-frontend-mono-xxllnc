// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

// @ts-ignore
import recordsTable from './recordsTable.svg';

const datastoreTitleHeight = 72;
const actionBarHeight = 48;

const tablePadding = '24px';
const tableMargin = '20px';
const cardPadding = '8px';
const tableSideSpace = `${tableMargin} + ${cardPadding}`;
const paginatorSideSpace = `${tableSideSpace} + ${tablePadding}`;

export const useDataTableStyles = makeStyles(
  ({
    typography,
    mintlab: { greyscale, radius },
    palette: { elephant },
  }: Theme) => ({
    wrapper: {
      width: '100%',
      height: `calc(100% - ${datastoreTitleHeight}px - ${actionBarHeight}px)`,
      position: 'relative',
      margin: '0 16px 0 0',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
    },
    sheet: {
      width: '100%',
      height: '100%',
      'background-image': `url(${recordsTable})`,
      'background-size': '18px',
      display: 'flex',
      position: 'relative',
      backgroundColor: greyscale.light,
      borderRadius: `${radius.sheet}px ${radius.sheet}px 0 0`,
    },
    tableWrapper: {
      fontFamily: typography.fontFamily,
      display: 'block',
      margin: `20px 20px 0 20px`,
      overflowY: 'scroll',
      width: '100%',
    },
    loader: {
      position: 'absolute',
      bottom: 1,
      width: '100%',
    },
    pagination: {
      width: `calc(100% - (${paginatorSideSpace}))`,
      position: 'fixed',
      bottom: '0px',
    },
    snack: {
      marginBottom: 35,
    },
    record: {
      display: 'flex',
      flexDirection: 'row',
      gap: 5,
    },
    recordPreview: {
      textDecoration: 'none',
      color: elephant.main,
    },
  })
);

export const useTableStyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { primary } }: Theme) => ({
    tableRow: {
      borderBottom: `1px solid ${greyscale.dark}`,
      alignItems: 'center',
      '&:hover': {
        backgroundColor: primary.lightest,
      },
    },
  })
);
