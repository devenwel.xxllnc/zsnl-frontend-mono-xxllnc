// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { TransactionType } from '../../Transactions.types';
import { getFormDefinition } from './Form.formDefintion';

type FormPropsType = {
  transaction: TransactionType;
};

const Form: React.ComponentType<FormPropsType> = ({ transaction }) => {
  const [t] = useTranslation('transactions');

  const formDefinition = getFormDefinition(t, transaction);

  const { fields } = useForm({
    formDefinition,
  });

  return (
    <div>
      {fields.map(
        ({ FieldComponent, name, error, touched, value, ...rest }) => {
          const restValues = {
            ...cloneWithout(rest, 'type', 'classes'),
          };

          return (
            <FormControlWrapper
              {...restValues}
              error={error}
              touched={touched}
              key={name}
            >
              <FieldComponent
                name={name}
                value={value}
                key={name}
                {...restValues}
              />
            </FormControlWrapper>
          );
        }
      )}
    </div>
  );
};

export default Form;
