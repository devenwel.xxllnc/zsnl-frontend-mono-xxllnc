// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ObjectTypeManagementRootStateType } from '../objectTypeManagement.reducer';
import { CreateStateType } from '../create/create.reducer';

export const createStateSelector = (
  state: ObjectTypeManagementRootStateType
): CreateStateType => state.objectTypeManagement.create;
