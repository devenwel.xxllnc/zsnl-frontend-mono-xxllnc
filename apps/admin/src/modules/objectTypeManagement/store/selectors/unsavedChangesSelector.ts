// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ObjectTypeManagementRootStateType } from '../objectTypeManagement.reducer';
import { UnsavedChangesStateType } from '../unsavedChanges/unsavedChanges.reducer';

export const unsavedChangesSelector = (
  state: ObjectTypeManagementRootStateType
): UnsavedChangesStateType => state.objectTypeManagement.unsavedChanges;
