// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const OBJECT_TYPE_FETCH = createAjaxConstants('OBJECT_TYPE_FETCH');
