// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const SET_OBJECT_TYPE_UNSAVED_CHANGES =
  'SET_OBJECT_TYPE_UNSAVED_CHANGES';
