// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useSelector } from 'react-redux';
import Select, { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { SortableList, useListStyle } from '@mintlab/ui/App/Zaaksysteem/List';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICatalog } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { objectTypeSelector } from '../../../../store/selectors/objectTypeSelector';

type ObjectType = {
  id: string;
  name: string;
};

const extractSelectedValue = (event: any) => event.target.value.value;

const fetchObjectTypeRelationChoices: (
  keyword: string
) => Promise<ValueType<ObjectType>[]> = async keyword => {
  const response = await request<APICatalog.SearchCatalogResponseBody>(
    'GET',
    buildUrl<APICatalog.SearchCatalogRequestParams>(
      '/api/v2/admin/catalog/search',
      { keyword, 'filter[type]': 'custom_object_type' }
    )
  );

  return response.data.map(({ attributes: { name }, id }) => {
    return {
      value: {
        id: id || '',
        name,
      },
      label: name,
    };
  });
};

export const ObjectTypeRelationList: React.ComponentType<
  FormRendererFormField<any, any, ObjectType[]>
> = props => {
  const { setFieldValue, name, placeholder } = props;
  const [input, setInput] = React.useState('');
  const classes = useListStyle();
  const { fields, add } = useMultiValueField<any, ObjectType>(props);
  const value = fields.map(field => field.value);
  const { objectType } = useSelector(objectTypeSelector);
  const editedObjectTypeUuid = objectType?.uuid;

  return (
    <div className={classes.listContainer}>
      <SortableList
        value={fields.map(field => ({ ...field, id: field.value.id }))}
        onReorder={reorderedFields =>
          setFieldValue(
            name,
            reorderedFields.map(field => field.value)
          )
        }
        renderItem={field => (
          <div className={classes.itemContainerSimple}>
            {field?.value?.name}
            <Button
              name="removeRelation"
              icon="close"
              iconSize="small"
              action={field.remove}
            />
          </div>
        )}
      />
      <DataProvider
        autoProvide={input !== ''}
        provider={fetchObjectTypeRelationChoices}
        providerArguments={[input]}
      >
        {props => {
          const { data, busy } = props;
          return (
            <Select
              variant="generic"
              name={name}
              loading={busy}
              onChange={event => add(extractSelectedValue(event))}
              startAdornment={<Icon size="small">{iconNames.search}</Icon>}
              isMulti={false}
              value={null}
              isClearable={false}
              choices={
                input.length > 2 && data
                  ? data.filter(
                      option =>
                        value.every(item => item.id !== option.value.id) &&
                        option.value.id !== editedObjectTypeUuid
                    )
                  : []
              }
              getChoices={setInput}
              placeholder={placeholder}
            />
          );
        }}
      </DataProvider>
    </div>
  );
};
