// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useDataStoreStyles = makeStyles(() => ({
  wrapper: {
    height: 'calc(100% - 45px)',
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    marginBottom: 30,
  },
}));
