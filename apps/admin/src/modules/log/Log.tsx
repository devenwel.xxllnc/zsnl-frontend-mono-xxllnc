// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import Typography from '@mui/material/Typography';
import SubAppHeader from '../../components/Header/SubAppHeader';
import { useDataStoreStyles } from './Log.style';
import { FiltersType } from './Log.types';
import { defaultFilters, getData } from './Log.library';
import ActionBar from './ActionBar/ActionBar';
import DataTable from './DataTable/DataTable';

const Log: React.ComponentType = () => {
  const classes = useDataStoreStyles();
  const [t] = useTranslation('log');
  const [filters, setFilters] = useState<FiltersType>(defaultFilters);

  const { data } = useQuery(['data', filters], () => getData(filters));

  return (
    <div className={classes.wrapper}>
      <SubAppHeader>
        <Typography variant="h3">{t('log')}</Typography>
      </SubAppHeader>
      <ActionBar filters={filters} setFilters={setFilters} />
      <DataTable data={data} filters={filters} setFilters={setFilters} />
    </div>
  );
};

export default Log;
