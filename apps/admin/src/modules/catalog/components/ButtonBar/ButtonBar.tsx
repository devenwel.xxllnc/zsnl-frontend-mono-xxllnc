// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import Render from '@mintlab/ui/App/Abstract/Render';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import DropdownMenu, {
  DropdownMenuList,
  //@ts-ignore
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import { withStyles } from '@mui/styles';
import { buttonBarStyleSheet } from './ButtonBar.style';

/**
 * @reactProps {Object} classes
 * @reactProps {Array} actionButtons
 * @reactProps {Array} advancedActionButtons
 * @reactProps {Array} permanentButtons
 * @return {ReactElement}
 */
const ButtonBar = ({
  classes,
  actionButtons,
  advancedActionButtons,
  permanentButtons,
}: any) => (
  <div className={classes.wrapper}>
    <Render condition={actionButtons.length || advancedActionButtons.length}>
      <div className={classes.segment}>
        <Render condition={actionButtons.length}>
          {actionButtons.map(({ action, tooltip, type, name }: any) => (
            <Tooltip key={type} title={tooltip} placement="bottom">
              <Button action={action} name={name} icon={type} />
            </Tooltip>
          ))}
        </Render>
        <Render condition={advancedActionButtons.length}>
          <DropdownMenu
            transformOrigin={{
              vertical: -50,
              horizontal: 'center',
            }}
            trigger={<Button icon="more_vert" name="buttonBarMenuTrigger" />}
          >
            <DropdownMenuList
              items={advancedActionButtons.map(({ action, title }: any) => ({
                action,
                label: title,
                scope: `catalog-header:button-bar:${title}`,
              }))}
            />
          </DropdownMenu>
        </Render>
      </div>
    </Render>
    <div className={classes.segment}>
      {permanentButtons.map(({ action, tooltip, type, name }: any) => (
        <Tooltip key={type} title={tooltip} placement="bottom">
          <Button action={action} icon={type} name={name} />
        </Tooltip>
      ))}
    </div>
  </div>
);

export default withStyles(buttonBarStyleSheet)(ButtonBar);
