// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const buttonBarStyleSheet = ({ mintlab: { greyscale } }) => ({
  wrapper: {
    display: 'flex',
    '&>div:not(:last-child):after': {
      content: '""',
      width: '1px',
      height: '30px',
      background: greyscale.darker,
      position: 'absolute',
      bottom: '10px',
      right: '0px',
    },
  },
  segment: {
    position: 'relative',
    display: 'flex',
  },
  dropdownMenuButton: {
    justifyContent: 'left',
  },
});
