// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/material';
import Button from '@mintlab/ui/App/Material/Button';
import Typography from '@mui/material/Typography';

const SCOPE = 'catalog:move-bar';

/**
 * @param {Function} cancelAction
 * @param {Function} moveAction
 * @param {*} numToMove
 * @param {Function} t
 * @param {boolean} [disabled=false]
 * @return {ReactElement}
 */
const MoveBar = ({
  t,
  numToMove,
  moveAction,
  cancelAction,
  disabled = false,
}) => {
  const {
    palette: { common, primary },
    typography,
    mintlab: { shadows },
  } = useTheme();
  return (
    <div style={{ width: 'calc(100% - 24px - 24px)', margin: '20px auto' }}>
      <div
        style={{
          flexGrow: '1',
          margin: 'auto',
          maxWidth: 'calc(1600px - 24px - 24px)',
          height: '42px',
          display: 'flex',
          padding: '16px 24px',
          backgroundColor: primary.main,
          borderRadius: '8px',
          zIndex: 100,
          boxShadow: shadows.medium,
        }}
        data-scope={SCOPE}
      >
        <div
          style={{
            display: 'flex',
            width: 'auto',
            flexDirection: 'column',
            alignItems: 'normal',
            justifyContent: 'center',
          }}
        >
          <Typography
            variant="h5"
            sx={{
              color: common.white,
              fontWeight: typography.fontWeightRegular,
              marginBottom: '2px',
            }}
          >
            {t('catalog:move:label', { numToMove, count: numToMove })}
          </Typography>
          <Typography
            variant="caption"
            sx={{ color: common.white, fontWeight: typography.fontWeightLight }}
          >
            {t('catalog:move:description', { count: numToMove })}
          </Typography>
        </div>
        <div
          style={{
            display: 'flex',
            gap: 20,
            flex: 'auto',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}
        >
          <Button
            action={cancelAction}
            name="moveBarCancel"
            sx={{
              color: common.white,
              fontWeight: typography.fontWeightRegular,
            }}
          >
            {t('dialog:cancel')}
          </Button>
          <Button
            name="moveBarExecute"
            action={moveAction}
            icon="folder_move"
            iconSize="small"
            disabled={disabled}
            sx={{
              backgroundColor: common.white,
              color: primary.main,
              gap: 1,
              '&:hover': {
                backgroundColor: common.white,
              },
            }}
          >
            {t('catalog:move:moveButton')}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default MoveBar;
