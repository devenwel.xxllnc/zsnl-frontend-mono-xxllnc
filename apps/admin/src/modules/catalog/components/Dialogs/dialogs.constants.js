// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const DIALOG_ATTRIBUTE = 'Attribute';
export const DIALOG_CHANGE_ONLINE_STATUS = 'ChangeOnlineStatus';
export const DIALOG_EMAIL_TEMPLATE = 'EmailTemplate';
export const DIALOG_FOLDER = 'Folder';
export const DIALOG_ADD_ELEMENT = 'AddElement';
export const DIALOG_CASE_TYPE_VERSIONS = 'CaseTypeVersions';
export const DIALOG_CASE_TYPE_VERSIONS_ACTIVATE = 'CaseTypeVersionsActivate';
export const DIALOG_CONFIRM_DELETE = 'ConfirmDelete';
