// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { asArray } from '@mintlab/kitchen-sink/source';
import { searchDocumentAttributes } from '../../../../../../library/requests/EmailTemplate';

const fetchDocumentAttributes = input =>
  searchDocumentAttributes(input)
    .then(attributes =>
      asArray(attributes).map(attribute => ({
        value: attribute.id,
        label: attribute.attributes.name,
      }))
    )
    .catch(() => {});

const DocumentAttributeSearcher = ({ ...restProps }) => {
  const [input, setInput] = useState('');

  return (
    <DataProvider
      providerArguments={[input]}
      autoProvide={input !== ''}
      provider={fetchDocumentAttributes}
    >
      {({ data, busy }) => {
        const normalizedChoices = data || [];
        return (
          <Select
            {...restProps}
            choices={normalizedChoices}
            isClearable={true}
            loading={busy}
            getChoices={setInput}
          />
        );
      }}
    </DataProvider>
  );
};

export default DocumentAttributeSearcher;
