// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikValues } from 'formik';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';

const Folder: React.FunctionComponent<any> = ({
  saveAction,
  formDefinition,
  id,
  t,
  hide,
}) => {
  const handleOnSubmit = (values: FormikValues) => {
    saveAction({ values });
  };

  const title = t('folder:dialog.title', {
    action: id ? t('common:edit') : t('common:create'),
  });

  return (
    <FormDialog
      formDefinition={formDefinition}
      onSubmit={handleOnSubmit}
      title={title}
      icon="folder"
      onClose={hide}
      scope="catalog-folder-dialog"
      isInitialValid={id ? true : false}
    />
  );
};

export default Folder;
