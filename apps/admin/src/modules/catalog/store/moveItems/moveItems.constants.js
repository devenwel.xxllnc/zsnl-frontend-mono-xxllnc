// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_MOVEITEMS_CONFIRM = createAjaxConstants(
  'CATALOG:MOVEITEMS:CONFIRM'
);
export const CATALOG_MOVEITEMS_START = 'CATALOG:MOVEITEMS:START';
export const CATALOG_MOVEITEMS_CLEAR = 'CATALOG:MOVEITEMS:CLEAR';
