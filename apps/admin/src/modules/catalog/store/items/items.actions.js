// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { queryToObject } from '../../library/searchQuery';
import {
  CATALOG_FETCH,
  CATALOG_TOGGLE_ITEM,
  CATALOG_CLEAR_SELECTED,
  CATALOG_RELOAD,
  CATALOG_ROUTE_CHANGE,
} from './items.constants';

const fetchAjaxAction = createAjaxAction(CATALOG_FETCH);

export const loadMore = payload => {
  const { id, query, page, page_size } = payload;

  if (query) {
    const queryObject = queryToObject(query);
    return fetchAjaxAction({
      payload,
      url: buildUrl('/api/v2/admin/catalog/search', {
        ...queryObject,
        page,
        page_size,
      }),
      method: 'GET',
    });
  }

  return fetchAjaxAction({
    payload,
    url: buildUrl('/api/v2/admin/catalog/get_folder_contents', {
      folder_id: id,
      page,
      page_size,
    }),
    method: 'GET',
  });
};

export const triggerReload = () => ({
  type: CATALOG_RELOAD,
});

export const routeChange = ({ id }) => ({
  type: CATALOG_ROUTE_CHANGE,
  payload: {
    id,
  },
});

export const toggleCatalogItem = (id, multiSelect = false) => ({
  type: CATALOG_TOGGLE_ITEM,
  payload: {
    id,
    multiSelect,
  },
});

export const catalogClearSelected = () => ({
  type: CATALOG_CLEAR_SELECTED,
});
