// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';

const getReturnUrl = () =>
  `/admin/catalogus${window.location.href.split('admin/catalogus')[1] || ''}`;

/**
 * @param {string} id
 * @param {string} suffix
 * @param {boolean} shouldReturn
 * @param {Object} params
 * @return {string}
 */
export const getPathToCaseType = (id, suffix, shouldReturn, params = {}) => {
  return buildUrl(`/admin/zaaktypen/${id}${suffix}`, {
    return_url: shouldReturn ? getReturnUrl() : undefined,
    ...params,
  });
};

/**
 * @param {string} id
 * @param {string} suffix
 * @param {boolean} shouldReturn
 * @param {Object} params
 * @return {string}
 */
export const getPathToObjectType = (id, suffix, shouldReturn, params = {}) =>
  buildUrl(`/admin/objecttypen/${id}${suffix}`, {
    return_url: shouldReturn ? getReturnUrl() : undefined,
    ...params,
  });

/**
 * @param {string} id
 * @return {string}
 */
export const getPathToCustomObjectType = ({ id, folder_uuid }) => {
  const return_url = getReturnUrl();

  return id
    ? buildUrl(`/admin/objecttype/edit/${id}`, { return_url })
    : buildUrl(`/admin/objecttype/create`, { folder_uuid, return_url });
};

/**
 * @return {string}
 */
export const getPathToImport = () =>
  buildUrl('/admin/object/1', {
    return_url: '/admin/catalogus/{}',
  });

/**
 * Get path to catalog item
 * @param {string} id
 * @param {string} type
 * @return {string}
 */
export const getPathToItem = (id, type = 'folder') => {
  const basePath = '/admin/catalogus';
  if (!id) {
    return basePath;
  }

  const paths = {
    folder: `${basePath}/${id}`,
    case_type: getPathToCaseType(id, '/bewerken', true),
    object_type: getPathToObjectType(id, '/bewerken', true),
    custom_object_type: getPathToCustomObjectType({ id }),
  };

  return paths[type];
};
