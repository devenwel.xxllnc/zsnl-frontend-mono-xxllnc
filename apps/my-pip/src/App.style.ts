// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

//@ts-ignore
export const useAppStyle = makeStyles(({ typography }) => ({
  app: {
    fontFamily: typography.fontFamily,
    height: '100%',
  },
}));
