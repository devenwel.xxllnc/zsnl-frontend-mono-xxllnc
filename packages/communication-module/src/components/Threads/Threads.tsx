// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Typography from '@mui/material/Typography';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { get } from '@mintlab/kitchen-sink/source';
import { ThreadType } from '../../types/Thread.types';
import ThreadsHeaderContainer from './ThreadsHeader/ThreadsHeaderContainer';
import ThreadList, { ThreadListPropsType } from './ThreadList/ThreadList';
import { useThreadsStyle } from './Threads.style';

export interface ThreadsPropsType extends Pick<ThreadListPropsType, 'context'> {
  busy: boolean;
  threadList: ThreadType[];
  fetchThreadList: () => void;
  showLinkToCase: boolean;
}

const filterThreadList = (
  threadList: ThreadType[],
  searchTerm: string,
  filter: string | null
): ThreadType[] | [] => {
  const matchField = (obj: any, field: string): boolean => {
    const value = get(obj, field);
    return typeof value === 'string'
      ? value.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
      : false;
  };

  return threadList
    .filter(thread => (filter === null ? true : thread.type === filter))
    .filter(thread =>
      searchTerm === ''
        ? true
        : [
            'summary',
            'withName',
            'lastMessage.createdByName',
            'lastMessage.subject',
          ].some(field => matchField(thread, field))
    );
};

const Threads: React.FunctionComponent<ThreadsPropsType> = ({
  threadList,
  fetchThreadList,
  busy,
  showLinkToCase,
  context,
}) => {
  const classes = useThreadsStyle();
  const [t] = useTranslation(['communication']);
  const [searchTerm, setSearchTerm] = useState('');
  const [filter, setFilter] = useState<null | string>(null);
  const filteredThreadList = filterThreadList(threadList, searchTerm, filter);
  const { identifier } = useParams();

  const renderList = () => (
    <div className={classes.listWrapper}>
      <ThreadList
        threads={filteredThreadList}
        context={context}
        showLinkToCase={showLinkToCase}
        selectedThreadId={identifier}
      />
    </div>
  );

  const renderNoContent = () =>
    busy ? null : (
      <div className={classes.noContentWrapper}>
        <Typography variant="body1" classes={{ root: classes.noContentText }}>
          {t('thread.noContent')}
        </Typography>
      </div>
    );

  const renderContent = () =>
    filteredThreadList.length > 0 ? renderList() : renderNoContent();

  useEffect(() => {
    fetchThreadList();
  }, []);

  return (
    <div className={classes.wrapper}>
      <ThreadsHeaderContainer
        filter={filter || ''}
        searchTerm={searchTerm}
        onFilterChange={setFilter}
        onSearchTermChange={setSearchTerm}
      />
      {busy && <Loader delay={200} />}
      <ErrorBoundary>{renderContent()}</ErrorBoundary>
    </div>
  );
};

export default Threads;
