// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useMessageTabsStyle = makeStyles(
  ({ typography, mintlab: { greyscale } }: any) => ({
    tabsRoot: {
      borderBottom: `1px solid ${greyscale.dark}`,
    },
    tabWrapper: {
      ...typography.title2,
      fontWeight: typography.fontWeightRegular,
    },
    indicator: {},
  })
);
