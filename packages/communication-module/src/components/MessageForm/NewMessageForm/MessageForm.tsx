// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import PlusButtonSpaceWrapper from '@zaaksysteem/common/src/components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
  TYPE_EXTERNAL_MESSAGE,
  TYPE_POSTEX,
} from '../../../library/communicationTypes.constants';
import SwitchViewButton from '../../shared/SwitchViewButton/SwitchViewButton';
import { TypeOfMessage } from '../../../types/Message.types';
import ScrollWrapper from '../../shared/ScrollWrapper/ScrollWrapper';
import ContactMomentContainer from './ContactMoment/ContactMomentContainer';
import NoteContainer from './Note/NoteContainer';
import ExternalMessage from './ExternalMessage/ExternalMessage';
import { useMessageFormStyle } from './MessageForm.style';
import TabsContainer from './Tabs/TabsContainer';
import Tabs from './Tabs/Tabs';

const components = {
  [TYPE_CONTACT_MOMENT]: ContactMomentContainer,
  [TYPE_NOTE]: NoteContainer,
  [TYPE_EXTERNAL_MESSAGE]: ExternalMessage,
  [TYPE_POSTEX]: ExternalMessage,
};

export type MessageFormPropsType = {
  classes: any;
  width: string;
  alwaysShowBackButton: boolean;
  canCreate: boolean;
  defaultSubType?: 'email' | 'pip' | 'postex';
};

type AddFormParamsType = {
  type: TypeOfMessage;
};

const AddForm: React.FunctionComponent<MessageFormPropsType> = ({
  width,
  alwaysShowBackButton,
  canCreate,
  defaultSubType,
}) => {
  const classes = useMessageFormStyle();
  const navigate = useNavigate();
  const { type } = useParams<keyof AddFormParamsType>() as AddFormParamsType;
  const ContainerElement = components[type];
  const showBackButton = alwaysShowBackButton || ['xs', 'sm'].includes(width);
  const ConnectedTab = TabsContainer(Tabs);
  const cancel = () => navigate('..');

  if (!canCreate) {
    return null;
  }

  return (
    <React.Fragment>
      {showBackButton && <SwitchViewButton />}
      <ScrollWrapper>
        <div className={classes.addFormWrapper}>
          <ConnectedTab type={type} />
          <ErrorBoundary>
            <PlusButtonSpaceWrapper>
              <ContainerElement
                cancel={cancel}
                //@ts-ignore
                type={type}
                defaultSubType={defaultSubType}
              />
            </PlusButtonSpaceWrapper>
          </ErrorBoundary>
        </div>
      </ScrollWrapper>
    </React.Fragment>
  );
};

export default AddForm;
