// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type FieldMapType = [string, string][];
export type FlatFormValues<Values> = { [key in keyof Values]?: string };
