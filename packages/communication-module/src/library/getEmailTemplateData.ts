// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  EmailIntegration,
  EmailTemplateDataType,
} from '../types/EmailIntegration.types';

export const getEmailTemplateData = (
  emailIntegrations: EmailIntegration[],
  htmlEmailTemplateName?: string
): EmailTemplateDataType | undefined => {
  // the admin could potentially configure multiple email integrations
  //   but we only support configuring one, so we pick the first
  const emailIntegration = emailIntegrations[0];

  if (!emailIntegration || !htmlEmailTemplateName) {
    return;
  }

  const emailTemplatesSettings =
    emailIntegration.instance.interface_config.rich_email_templates;

  // the only way to know which template is relevant for the case (if any)
  //   is to match it to the label returned in the case call
  // note: the template set in the case might have been removed from the integration
  const emailTemplateSettings = emailTemplatesSettings.find(
    config => config.label === htmlEmailTemplateName
  );

  if (!emailTemplateSettings) {
    return;
  }

  const { label, template, image } = emailTemplateSettings;
  // only one image can be uploaded, but it's returned as an array
  const imageUuid = image[0].uuid;

  return {
    label,
    template,
    imageUuid,
  };
};
