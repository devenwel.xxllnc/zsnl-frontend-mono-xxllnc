// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export { default as Table } from '@mui/material/Table';
export { default as TableRow } from './row/TableRow';
export { default as TableCell } from './cell/TableCell';
export { default as TableHead } from '@mui/material/TableHead';
export { default as TableBody } from '@mui/material/TableBody';
export { default as TableFooter } from '@mui/material/TableFooter';
