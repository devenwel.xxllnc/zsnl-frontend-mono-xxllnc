// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Checkbox } from './Checkbox';
export * from './Checkbox';
export default Checkbox;
