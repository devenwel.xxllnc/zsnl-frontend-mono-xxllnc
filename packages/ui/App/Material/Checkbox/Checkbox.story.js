// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, select, boolean } from '../../story';
import Checkbox from '.';

const stores = {
  Default: {
    checked: false,
  },
};

stories(
  module,
  __dirname,
  {
    Default({ store, checked }) {
      const onChange = ({ target }) => {
        store.set({
          checked: target.checked,
        });
      };
      return (
        <Checkbox
          checked={checked}
          disabled={boolean('Disabled', false)}
          label="test"
          name="foobar"
          color={select(
            'Color',
            ['primary', 'secondary', 'default'],
            'primary'
          )}
          onChange={onChange}
        />
      );
    },
  },
  stores
);
