// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import toJson from 'enzyme-to-json';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import Checkbox from './Checkbox';

/**
 * @test {Checkbox}
 */
describe('The `Checkbox` component', () => {
  test('renders correctly', () => {
    const component = shallow(
      <Checkbox
        color="secondary"
        defaultChecked={true}
        disabled={true}
        name="MyFancyCheckbox"
      />
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
