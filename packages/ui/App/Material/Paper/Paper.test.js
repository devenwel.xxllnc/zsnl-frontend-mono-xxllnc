// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import { mount } from 'enzyme';
import Paper from '.';

/**
 * @test {Paper}
 */
describe('The `Paper` component', () => {
  test('passes all props to its child component', () => {
    const wrapper = mount(
      <Paper elevation={13} square={true}>
        :-)
      </Paper>
    );
    const actual = wrapper.find('Paper').props();
    const asserted = {
      children: ':-)',
      elevation: 13,
      square: true,
    };

    expect(actual).toMatchObject(asserted);
  });
});
