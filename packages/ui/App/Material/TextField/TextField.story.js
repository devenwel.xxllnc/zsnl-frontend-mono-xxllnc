// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, text, boolean, select } from '../../story';
import Icon from '../../Material/Icon';
import { Button } from '../Button/Button';
import TextField from '.';

const stores = {
  TextField: {
    value: '',
  },
};

/**
 * @param {SyntheticEvent} event
 * @param {Object} store
 * @return {undefined}
 */
const onChange = ({ target: { value } }, store) => {
  store.set({ value });
};

stories(
  module,
  __dirname,
  {
    TextField: function TestStory({ store, value }) {
      const multiline = boolean('Multiline', false);
      const placeholder = text('Placeholder', 'Placeholder');
      const startAdornment = boolean('Start adornment', false);
      const endAdornment = boolean('End adornment', false);
      const StartAdornment = (
        <Button label="test" color="inherit">
          search
        </Button>
      );
      const EndAdornment = <Icon size="small">search</Icon>;
      const action = boolean('Close action', false);
      const variant = select(
        'Variant',
        ['form', 'generic1', 'generic2'],
        'form'
      );
      const formatType = select('Formatting', [
        { label: 'none', value: null },
        { label: 'Preset, Euro', value: 'eurCurrency' },
        {
          label: 'Custom, Dollar',
          value: {
            prefix: '$',
            thousandSeparator: ',',
            decimalSeparator: '.',
            fixedDecimalScale: true,
            isNumericString: true,
            allowNegative: false,
            decimalScale: 2,
          },
        },
      ]);

      return (
        <div
          style={{
            width: '300px',
          }}
        >
          <TextField
            value={value}
            variant={variant}
            placeholder={placeholder}
            isMultiline={multiline}
            info={text('Info', 'Type ahead')}
            label={text('Label', 'Text field')}
            error={text('Error', '')}
            onChange={evt => onChange(evt, store)}
            scope="story"
            readOnly={boolean('readOnly', false)}
            required={true}
            formatType={formatType?.value}
            {...(startAdornment && { startAdornment: StartAdornment })}
            {...(endAdornment && { endAdornment: EndAdornment })}
            {...(action && {
              closeAction: () => {
                console.log('close');
              },
            })}
          />
        </div>
      );
    },
  },
  stores
);
