# 🔌 `Snackbar` component

> *Material Design* **Snackbar**.

## Usage

Shows a Snackbar with the supplied message. Pass in the configuration props (see the above documentation) to customize a snackbar. Only the `message` prop is required.

If a snackbar would be triggered while an existing snackbar is visible, the existing snackbar will animate out before the new snackbar is shown.

## Example

    <Snackbar 
      message='This is a test message'
      anchorOrigin={
        vertical: 'bottom',
        horizontal: 'left',
      }
      autoHideDuration={4000}
    />

## See also

- [`Snackbar` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Snackbar)
- [`Snackbar` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-snackbar)

## External resources

- [*Material Guidelines*: Snackbars](https://material.io/design/components/snackbars.html)
- [*Material-UI* `Snackbar` API](https://material-ui.com/api/snackbar/)
