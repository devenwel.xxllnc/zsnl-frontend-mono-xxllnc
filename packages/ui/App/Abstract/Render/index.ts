// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Render } from './Render';
export * from './Render';
export default Render;
