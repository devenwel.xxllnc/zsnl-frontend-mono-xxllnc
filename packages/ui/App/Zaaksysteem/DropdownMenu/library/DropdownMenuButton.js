// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import MuiButton from '@mui/material/Button';
import { useTheme } from '@mui/material';
import Icon from '../../../Material/Icon';
import { addScopeAttribute } from '../../../library/addScope';

/**
 * @param {Action} props
 * @param {Function} props.action
 * @param {string} props.icon
 * @param {string} props.label
 * @param {string} props.scope
 * @return {ReactElement}
 */
const DropdownMenuButton = React.forwardRef(
  ({ action, icon, label, scope, key }, ref) => {
    const labelComponent = icon ? (
      <Fragment>
        <Icon size="small">{icon}</Icon>
        {label}
      </Fragment>
    ) : (
      label
    );

    const {
      palette: { primary },
      typography,
      mintlab: { greyscale, radius },
    } = useTheme();

    return (
      <MuiButton
        key={key}
        ref={ref}
        onClick={action}
        fullWidth={false}
        {...addScopeAttribute(scope, 'button')}
        sx={{
          borderRadius: radius.dropdownButton,
          color: greyscale.offBlack,
          '&:hover': {
            backgroundColor: `${greyscale.offBlack}0D`,
            color: primary.main,
          },
          height: '36px',
          padding: '6px 20px 6px 11px',
          justifyContent: 'flex-start',
          '& > span:first-child > svg': {
            marginRight: '18px',
          },
          fontWeight: typography.fontWeightRegular,
        }}
      >
        {labelComponent}
      </MuiButton>
    );
  }
);

DropdownMenuButton.displayName = 'DropdownMenuButton';

export default DropdownMenuButton;
