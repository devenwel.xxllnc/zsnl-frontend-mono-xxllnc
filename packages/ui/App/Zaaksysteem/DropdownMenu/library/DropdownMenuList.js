// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Divider from '@mui/material/Divider';
import { addScopeProp } from '../../../library/addScope';
import DropdownMenuButton from './DropdownMenuButton';

const { isArray } = Array;

/**
 * Returns a wrapper with one or more groups of DropdownMenuButton components.
 * Groups are seperated by a Divider component.
 *
 * @param {Array<Action>|Array<Array<Action>>} items
 * @param {Object} classes
 * @return {ReactElement}
 */
const DropdownMenuList = React.forwardRef(({ items }, ref) => {
  const normalized = items.every(item => isArray(item)) ? items : [items];

  return normalized.map((group, index) => (
    <React.Fragment key={index}>
      {Boolean(index) && index !== normalized.length && <Divider />}
      {group.map(({ label, action, icon, scope }, buttonIndex) => (
        <DropdownMenuButton
          ref={ref}
          key={buttonIndex}
          label={label}
          action={action}
          icon={icon}
          {...addScopeProp(scope, label)}
        />
      ))}
    </React.Fragment>
  ));
});

DropdownMenuList.displayName = 'DropdownMenuList';

export default DropdownMenuList;
