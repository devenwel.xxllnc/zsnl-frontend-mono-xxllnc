// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
// @ts-nocheck

import * as React from 'react';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { TableRowProps } from 'react-virtualized';

/* eslint complexity: [2, 18] */
export default function defaultRowRenderer({
  className,
  columns,
  index,
  key,
  onRowClick,
  onRowDoubleClick,
  onRowMouseOut,
  onRowMouseOver,
  onRowRightClick,
  rowData,
  style,
  ...rest
}: TableRowProps) {
  const a11yProps = { 'aria-rowindex': index + 1 };
  const restProps = cloneWithout(rest, 'isScrolling');

  if (
    onRowClick ||
    onRowDoubleClick ||
    onRowMouseOut ||
    onRowMouseOver ||
    onRowRightClick
  ) {
    a11yProps['aria-label'] = 'row';
    a11yProps.tabIndex = 0;

    if (onRowClick) {
      a11yProps.onClick = event => onRowClick({ event, index, rowData });
    }
    if (onRowDoubleClick) {
      a11yProps.onDoubleClick = event =>
        onRowDoubleClick({ event, index, rowData });
    }
    if (onRowMouseOut) {
      a11yProps.onMouseOut = event => onRowMouseOut({ event, index, rowData });
    }
    if (onRowMouseOver) {
      a11yProps.onMouseOver = event =>
        onRowMouseOver({ event, index, rowData });
    }
    if (onRowRightClick) {
      a11yProps.onContextMenu = event =>
        onRowRightClick({ event, index, rowData });
    }
  }

  return (
    <div
      {...a11yProps}
      className={className}
      key={key}
      role="row"
      {...restProps}
      style={style}
    >
      {columns}
    </div>
  );
}
