// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SortDirection } from 'react-virtualized';
import { SortDirectionType } from 'react-virtualized';
import { RowType, ColumnType } from './types/SortableTableTypes';

/* eslint complexity: [2, 14] */
export const getSortedRows = (
  rows: RowType[],
  sortBy: string,
  sortDirection: SortDirectionType,
  sortInternal: boolean
) => {
  if (!sortBy || sortInternal === false) return rows;

  const sorted = rows.sort((sortA, sortB) => {
    const noValue = (first: RowType, second: RowType) => !first || !second;
    const parsePossibleDate = (input: any) => {
      if (input instanceof Date) return input;

      if (input.match(/[-|/]/g)?.length === 2) {
        const stripped = input.replace('/[-|/]/g', '');
        if (/^[0-9]*$/.test(stripped)) {
          return new Date(input);
        }
      }
      return null;
    };

    if (noValue(sortA, sortB)) {
      return 0;
    }

    const valA = (sortA as any)[sortBy];
    const valB = (sortB as any)[sortBy];

    if (!valA || (!valA && !valB)) {
      return -1;
    }

    if (valA && !valB) {
      return 1;
    }

    if (Number.isInteger(valA)) {
      return valA > valB ? 1 : -1;
    }

    if (parsePossibleDate(valA)) {
      const dateA = parsePossibleDate(valA);
      const dateB = parsePossibleDate(valB);

      if (!dateA || !dateB) return -1;
      return dateA.getTime() > dateB.getTime() ? 1 : -1;
    }

    return valA.toLowerCase().localeCompare(valB.toLowerCase());
  });

  return sortDirection === SortDirection.DESC ? sorted.reverse() : sorted;
};

export const reorder = (
  list: any[],
  startIndex: number,
  endIndex: number
): any[] => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export const getDefaultSorting = (columns: ColumnType[]) => {
  const [defaultColumn] = columns.filter(thisColumn => thisColumn.defaultSort);
  return defaultColumn?.name || '';
};
