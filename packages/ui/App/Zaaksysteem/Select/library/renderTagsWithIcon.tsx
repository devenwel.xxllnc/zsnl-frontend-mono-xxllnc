// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import Chip from '@mui/material/Chip';
import React from 'react';
import Icon, { iconNames } from '../../../Material/Icon';

export const renderTagsWithIcon =
  (
    iconName:
      | keyof typeof iconNames
      | ((option: any) => keyof typeof iconNames),
    iconProps: any = {}
  ) =>
  (value: any, getTagProps: any) =>
    //@ts-ignore
    value.map((option: any, index: number) => {
      const name = typeof iconName === 'string' ? iconName : iconName(option);
      return (
        // eslint-disable-next-line react/jsx-key
        <Chip
          sx={{
            margin: '0 3px',
            '& .MuiChip-label': { paddingLeft: '8px' },
            '& .MuiSvgIcon-root': { fontSize: '20px' },
          }}
          icon={
            <Icon
              style={{ height: 'unset', marginLeft: '10px' }}
              {...iconProps}
            >
              {name}
            </Icon>
          }
          variant="outlined"
          label={option.label}
          {...getTagProps({ index })}
        />
      );
    });
