// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useDebouncedCallback } from 'use-debounce';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '../../Material/TextField';
import { BaseSelectPropsType } from './types/BaseSelectPropsType';
import defaultFilterOptions from './library/filterOption';
import { getNormalizedValue } from './library/getNormalizedValue';

const defaultStyling = {
  '& .MuiAutocomplete-inputRoot': {
    padding: '5px 10px 2px 10px',
  },
  '& .MuiAutocomplete-inputRoot.Mui-disabled': {
    backgroundColor: 'transparent',
    pointerEvents: 'none',
  },
};

const genericStyling = {
  '& .MuiFilledInput-root': {
    paddingTop: '2px',
    paddingBottom: '2px',
    paddingLeft: '20px',
    color: 'rgb(47, 51, 56)',
    fontWeight: 100,
  },
  '&&& .Mui-focused': {
    backgroundColor: 'rgb(225 237 255)',
    '& :hover': {
      backgroundColor: 'rgb(225 237 255)',
    },
  },
};

export const FormSelect: React.ComponentType<
  BaseSelectPropsType<any> & {
    autoLoad?: boolean;
    nestedValue?: boolean;
    variant?: 'generic' | 'default';
    preventInputUpdateOnOptionSelection?: boolean;
    inputChangeDebounce?: { duration: number; minLength: number };
  }
  // eslint-disable-next-line complexity
> = ({
  disabled,
  error,
  loading,
  isClearable,
  isMulti = false,
  value,
  choices,
  getChoices,
  filterOption,
  name,
  onBlur,
  onChange,
  openMenuOnClick = true,
  isOptionDisabled,
  placeholder,
  renderOption,
  renderTags,
  freeSolo,
  startAdornment,
  autoLoad = false,
  nestedValue = false,
  sx = {},
  variant = 'default',
  preventInputUpdateOnOptionSelection = false,
  inputChangeDebounce = { duration: 300, minLength: 0 },
}) => {
  const [open, setOpen] = React.useState(false);
  const norm = getNormalizedValue({ value, isMulti, choices });
  const memValue = React.useMemo(
    () => norm,
    [isMulti ? norm.map((norm: any) => norm.label).join('') : norm.label]
  );
  const [inputValue, setInputValue] = React.useState(
    preventInputUpdateOnOptionSelection ? memValue?.label || '' : undefined
  );

  // eslint-disable-next-line complexity
  const [fetchChoices] = useDebouncedCallback((ev, val) => {
    if ((!autoLoad && open) || autoLoad) {
      if (val.length >= inputChangeDebounce.minLength) {
        getChoices?.(val, norm);
      }
    }
  }, inputChangeDebounce.duration);

  return (
    <Autocomplete
      filterSelectedOptions
      sx={{
        ...(variant === 'default' ? defaultStyling : genericStyling),
        ...sx,
      }}
      onOpen={() => setOpen(true)}
      onClose={() => setOpen(false)}
      value={memValue}
      inputValue={inputValue}
      options={choices || []}
      getOptionDisabled={isOptionDisabled}
      disableClearable={!isClearable}
      disabled={disabled}
      loading={loading}
      multiple={isMulti}
      freeSolo={freeSolo}
      openOnFocus={openMenuOnClick}
      renderTags={renderTags}
      renderOption={
        renderOption ||
        ((props, option, index) => {
          return (
            <li
              {...props}
              //@ts-ignore
              key={option.key || option.label + props['data-option-index']}
            >
              {option.label}
            </li>
          );
        })
      }
      isOptionEqualToValue={(option, value) => {
        return option.key && value.key
          ? option.key === value.key
          : value.label === option.label;
      }}
      onChange={(ev, value) => {
        if (onChange) {
          onChange({
            ...ev,
            target: {
              ...ev.target,
              name,
              value: nestedValue ? value?.value : value,
            },
          });
        }
      }}
      onInputChange={(ev, val, reason) => {
        if (preventInputUpdateOnOptionSelection && reason === 'input') {
          setInputValue(val);
        }
        reason === 'input' && fetchChoices(ev, val);
      }}
      renderInput={props => (
        <TextField
          onBlur={onBlur}
          name={name}
          error={error}
          placeholder={placeholder}
          {...props}
          InputProps={{
            ...props.InputProps,
            disableUnderline: variant === 'generic',
            ...(startAdornment ? { startAdornment } : {}),
          }}
        />
      )}
      filterOptions={(options, state) =>
        options.filter(opt =>
          (filterOption || defaultFilterOptions)(opt, state.inputValue)
        )
      }
    />
  );
};

export default FormSelect;
