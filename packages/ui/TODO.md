# TODO

- upgrade webpack to version 4
    - upgrade dependencies
    - make sure there's no outdated transitive browserslist package left
    - make the package.json browsers field more specific, e.g.
        - `not dead`
        - `major` key word
        - version operator
- factor out shared webpack configuration for build and storybook
    - node/storybook/webpack
    - node/webpack/config
- `dynamic-import-node` doesn't work in the test env if `--coverage` is passed
