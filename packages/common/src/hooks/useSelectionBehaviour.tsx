// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useEffect, useState, Dispatch, SetStateAction } from 'react';

import {
  TableRowProps,
  RowMouseEventHandlerParams,
  SortableTablePropsType,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';

type OnPageSelectFunctionType = (
  event: React.MouseEvent<any>,
  isSelected: boolean,
  selected: string[]
) => void;

type OnSelectAllFunctionType = (event: React.MouseEvent<any>) => void;

export type UseSelectionBehaviourPropsType = {
  afterPageSelected?: OnPageSelectFunctionType;
  afterRowClick?: (params: RowMouseEventHandlerParams) => void;
  afterPageParamChange?: () => void;
  afterAllSelected?: (event: React.MouseEvent<any>, selected: boolean) => void;
  page?: number;
  resultsPerPage?: number;
  selectAllTranslations?: SortableTablePropsType['selectAllTranslations'];
};

export type useSelectionBehaviourReturnType = {
  selected: string[];
  setSelected: Dispatch<SetStateAction<string[]>>;
  pageSelected: boolean;
  setPageSelected: Dispatch<SetStateAction<boolean>>;
  onRowClick: TableRowProps['onRowClick'];
  onSelectPage: OnPageSelectFunctionType;
  allSelected: boolean;
  setAllSelected: Dispatch<SetStateAction<boolean>>;
  onSelectAll: OnSelectAllFunctionType;
  selectAllTranslations: SortableTablePropsType['selectAllTranslations'];
  selectable: boolean;
  resetAll: () => void;
};

const hasValue = (value: any) => value !== undefined && value !== null;

export const useSelectionBehaviour = ({
  afterPageSelected,
  afterRowClick,
  afterPageParamChange,
  afterAllSelected,
  page,
  resultsPerPage,
  selectAllTranslations,
}: UseSelectionBehaviourPropsType): useSelectionBehaviourReturnType => {
  const [selected, setSelected] = useState<string[]>([]);
  const [pageSelected, setPageSelected] = useState(false);
  const [allSelected, setAllSelected] = useState(false);
  const noDuplicates = (value: any, index: number, array: any[]) =>
    array.indexOf(value) == index;

  useEffect(() => {
    if (hasValue(page) && hasValue(resultsPerPage)) {
      resetAll();
      afterPageParamChange && afterPageParamChange();
    }
  }, [page, resultsPerPage]);

  const onSelectPage: OnPageSelectFunctionType = (
    event,
    isSelected,
    selected
  ) => {
    setPageSelected(isSelected);
    if (isSelected) {
      setAllSelected(false);
      setSelected(selected);
    } else {
      setSelected([]);
    }
    afterPageSelected && afterPageSelected(event, isSelected, selected);
  };

  const onSelectAll: OnSelectAllFunctionType = event => {
    setAllSelected(!allSelected);
    afterAllSelected && afterAllSelected(event, !allSelected);
  };

  const onRowClick: TableRowProps['onRowClick'] = (
    params: RowMouseEventHandlerParams
  ) => {
    const { rowData } = params;

    if (selected.includes(rowData.uuid)) {
      setSelected(selected.filter(item => item !== rowData.uuid));
    } else {
      setSelected([...selected, rowData.uuid].filter(noDuplicates));
    }

    if (pageSelected) {
      setPageSelected(false);
    }
    afterRowClick && afterRowClick(params);
  };

  const resetAll = () => {
    setSelected([]);
    setPageSelected(false);
    setAllSelected(false);
  };

  return {
    selected,
    setSelected,
    pageSelected,
    setPageSelected,
    onSelectPage,
    onRowClick,
    allSelected,
    setAllSelected,
    onSelectAll,
    selectAllTranslations,
    selectable: true,
    resetAll,
  };
};
