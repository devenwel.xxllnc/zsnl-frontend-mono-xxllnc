// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import errorMiddleware, {
  ErrorMiddlewareOptionsType,
} from './error.middleware';

export const getErrorModule = (options: ErrorMiddlewareOptionsType) => ({
  id: 'error',
  middlewares: [errorMiddleware(options)],
});

export default getErrorModule;
