// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useSelector } from 'react-redux';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { getBagPreferences } from '@zaaksysteem/common/src/store/session/session.selectors';
import { IntegrationContextType } from '@mintlab/ui/types/MapIntegration';
import { FormFieldComponentType } from '../../types/Form2.types';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { AddressValueType } from './Address.types';
import { fetchAddressChoices, fetchAddressLookup } from './Address.library';

export const AddressBoxField: FormFieldComponentType<
  AddressValueType,
  { context: IntegrationContextType }
> = props => {
  const { value, name, onChange, readOnly } = props;
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [input, setInput] = React.useState('');
  const [preferredMunicipalities, preferredOnly] =
    useSelector(getBagPreferences);

  return (
    <React.Fragment>
      <DataProvider
        providerArguments={[
          input,
          (preferredMunicipalities || '').toString(),
          preferredOnly,
        ]}
        autoProvide={input !== ''}
        provider={fetchAddressChoices(openServerErrorDialog)}
      >
        {({ data, busy }) => {
          return readOnly ? (
            <div style={{ paddingBottom: 10 }}>
              <ReadonlyValuesContainer value={value?.address.full} />
            </div>
          ) : (
            <Select
              {...props}
              value={value ? { label: value.address.full, value } : null}
              choices={data || []}
              isClearable={true}
              loading={busy}
              getChoices={setInput}
              onChange={event => {
                const selectedPdokSuggestionId = event.target.value?.value;
                if (selectedPdokSuggestionId) {
                  fetchAddressLookup(selectedPdokSuggestionId).then(value => {
                    onChange({
                      target: {
                        name,
                        value,
                      },
                    });
                  });
                } else {
                  onChange({
                    target: { name, value: null },
                  });
                }
              }}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};
