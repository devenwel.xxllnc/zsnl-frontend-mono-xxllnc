// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  CreatableSelect as Select,
  ValueType,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

const MultiValueText: FormFieldComponentType<
  ValueType<string>[],
  any
> = props => {
  const { multiValue, value, readOnly, config, ...rest } = props;

  return readOnly ? (
    <ReadonlyValuesContainer value={(value || []).map(val => val.label)} />
  ) : (
    <Select
      {...rest}
      {...config}
      value={value}
      isMulti={multiValue}
      isClearable={true}
    />
  );
};

export default MultiValueText;
