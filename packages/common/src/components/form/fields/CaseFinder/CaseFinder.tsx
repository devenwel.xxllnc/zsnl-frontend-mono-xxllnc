// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchCaseChoices } from './CaseFinder.library';

const CaseFinder: FormFieldComponentType<ValueType<string>> = props => {
  const { choices, filter } = props;
  const [input, setInput] = useState('');
  const [t] = useTranslation('common');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  return (
    <React.Fragment>
      <DataProvider
        providerArguments={[input, filter?.status]}
        autoProvide={input !== ''}
        provider={fetchCaseChoices(t, openServerErrorDialog)}
      >
        {({ data, busy }) => {
          const normalizedChoices = data || choices;

          return (
            <FlatValueSelect
              {...props}
              choices={normalizedChoices}
              isClearable={true}
              loading={busy}
              getChoices={setInput}
              components={{
                Option: MultilineOption,
              }}
              filterOption={() => true}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default CaseFinder;
