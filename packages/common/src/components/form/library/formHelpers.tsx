// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikProps } from 'formik';
import {
  FormRendererFormField,
  FormRendererFieldComponentPropsType,
  NestedFormValue,
  FormValue,
} from '../types/formDefinition.types';

export function createField<Values>(fields: FormRendererFormField<Values>[]) {
  const Field: React.ComponentType<
    FormRendererFieldComponentPropsType<Values>
  > = ({ name, error }) => {
    const [{ FieldComponent, ...restProps }] = fields.filter(
      item => item.name === name
    );
    return (
      <React.Fragment>
        <FieldComponent {...restProps} error={error} />
      </React.Fragment>
    );
  };
  return Field;
}

export function getField<Values = any>(
  fields: FormRendererFormField<Values>[]
) {
  return (
    fieldName: keyof Values
  ): FormRendererFormField<Values> | undefined => {
    return fields.find(field => fieldName === field.name);
  };
}

export const setTouchedAndHandleChange =
  ({
    setFieldTouched,
    handleChange,
  }: Pick<FormikProps<any>, 'handleChange' | 'setFieldTouched'>) =>
  (event: React.ChangeEvent<any>) => {
    const {
      target: { name },
    } = event;
    setFieldTouched(name, true);
    handleChange(event);
  };

export function hasField<Values = any>(
  fields: FormRendererFormField<Values>[]
) {
  return (fieldName: keyof Values): boolean => {
    return fields.findIndex(field => fieldName === field.name) > -1;
  };
}

const flattenSingleValueField = (value: unknown, nestedKey: string = 'value') =>
  (value as NestedFormValue)[nestedKey]
    ? (value as NestedFormValue)[nestedKey].toString()
    : (value as FormValue).toString();

export const flattenField = (
  value: unknown | unknown[],
  nestedKey: string = 'value'
): string =>
  Array.isArray(value)
    ? value.map(item => flattenSingleValueField(item, nestedKey)).join(', ')
    : flattenSingleValueField(value, nestedKey);
