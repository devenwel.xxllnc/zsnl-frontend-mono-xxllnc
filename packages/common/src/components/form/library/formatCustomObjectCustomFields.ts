// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type CustomFieldsType = {
  [key: string]: any;
};

type FormatCustomObjectCustomFields = (customFields: CustomFieldsType) => any;

/* eslint complexity: [2, 12] */
const formatCustomObjectCustomFields: FormatCustomObjectCustomFields =
  customFields =>
    Object.keys(customFields).reduce((acc, key) => {
      const customField = customFields[key];

      if (!customField) return acc;

      const { type, value } = customField;

      if (type === 'document') {
        return {
          ...acc,
          [key]: {
            type,
            value: value.map((file: any) => ({
              label: file.label,
              value: file.value,
              key: file.value,
            })),
          },
        };
      } else {
        return {
          ...acc,
          [key]: customFields[key],
        };
      }
    }, {});

export default formatCustomObjectCustomFields;
