// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import classnames from 'classnames';
//@ts-ignore
import Fab from '@mintlab/ui/App/Material/Fab';
import { useViewerControlStyles } from './ViewerControls.style';

interface ViewerControlsPropsType {
  scale: number;
  /* Defaults to 0.2 */
  scaleStep?: number;
  rotation?: number;
  className?: string;

  onScaleChange: (scale: number) => void;
  onRotationChange?: (rotation: number) => void;
}

export const ViewerControls: React.ComponentType<ViewerControlsPropsType> = ({
  onRotationChange,
  onScaleChange,
  rotation = 0,
  scale,
  className,
  scaleStep = 0.2,
}) => {
  const [t] = useTranslation('common');
  const classes = useViewerControlStyles();
  const zoomIn = () => onScaleChange(Math.min(5, scale + scaleStep));
  const zoomOut = () =>
    onScaleChange(Math.max(scaleStep * 2, scale - scaleStep));
  const rotate = () => {
    const newRotation = rotation + 90;
    onRotationChange && onRotationChange(newRotation >= 360 ? 0 : newRotation);
  };

  return (
    <div className={classnames(classes.wrapper, className)}>
      <Fab
        sx={{ marginBottom: '10px' }}
        aria-label={t('filePreview.zoomIn')}
        size="medium"
        action={zoomIn}
      >
        zoom_in
      </Fab>
      <Fab aria-label={t('filePreview.zoomOut')} size="medium" action={zoomOut}>
        zoom_out
      </Fab>
      {onRotationChange && (
        <Fab
          aria-label={t('filePreview.rotateRight')}
          size="small"
          action={rotate}
        >
          rotate_right
        </Fab>
      )}
    </div>
  );
};
