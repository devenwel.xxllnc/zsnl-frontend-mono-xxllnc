// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const TYPE_COMPANY = 'company';
export const TYPE_EMPLOYEE = 'employee';
export const TYPE_PERSON = 'person';
