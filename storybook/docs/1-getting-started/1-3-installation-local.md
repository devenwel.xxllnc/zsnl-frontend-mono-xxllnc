# Installation Local

## 🏃 Get it up and running using local Node.js

> Running this project using local `Node` gives the best developer experience.

## Prerequisites

Before you continue, **make sure you have Node ^10.x and Yarn ^1.22.x installed!**

## Local usage

You can also run the apps locally and have docker proxy traffic to your local instance.

### Zaaksysteem docker-compose development override

Cf. `../zaaksysteem/docker-compose.override.yml`

MacOS:

```
services:
  frontend-mono:
     build:
        context: "../zaaksysteem-frontend-mono"
        target: "base"
     environment:
        - PROXY_HOST=host.docker.internal
     depends_on:
        - frontend
     command: nginx -g 'daemon off;'
```

Linux:

```
services:
  frontend-mono:
     build:
        context: "../zaaksysteem-frontend-mono"
        target: "base"
     environment:
        - PROXY_HOST=172.17.0.1
     depends_on:
        - frontend
     command: nginx -g 'daemon off;'
```

### Build

```bash
$ cd /path/to/zaaksysteem
$ docker-compose build frontend-mono

```

### Starting

First we need to download all the dependencies and start the apps

```bash
$ cd /path/to/zaaksysteem-frontend-mono
$ yarn
$ yarn lerna:start

```

Then we can start the docker container which is used to proxy traffic to your local instanc

```bash
$ cd /path/to/zaaksysteem
$ docker-compose up -d frontend-mono

```
